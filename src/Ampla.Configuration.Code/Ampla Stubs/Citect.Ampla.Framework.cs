﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Citect.Ampla.Data;

// ReSharper disable CheckNamespace
namespace Citect.Ampla.Framework
// ReSharper restore CheckNamespace
{
    public class Project
    {
        public bool HasChanges
        {
            get { throw new System.NotImplementedException(); }
            set { throw new System.NotImplementedException(); }
        }

        public ItemTypeInfoCollection ItemTypeInfos
        {
            get { throw new System.NotImplementedException(); }
            set { throw new System.NotImplementedException(); }
        }

        public List<Item> RootItems
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public IEnumerable<Item> AllItems
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public Item this[string fullName]
        {
            get { throw new NotImplementedException(); }
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<object> GetItemsByType(Type itemType)
        {
            throw new NotImplementedException();
        }

        public Item AddNew(Type itemType, Guid newGuid, string name, ChangeContext changeContext)
        {
            throw new NotImplementedException();
        }
    }

    public class Item
    {
        public string DisplayOrder
        {
            get { throw new System.NotImplementedException(); }
            set { throw new System.NotImplementedException(); }
        }

        public List<Item> Items
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public string FullName
        {
            get { throw new NotImplementedException(); }
        }

        public static bool IsNameValid(string name)
        {
            throw new NotImplementedException();
        }

        public ItemPropertyDescriptor FindPropertyDescriptor(string property)
        {
            throw new NotImplementedException();
        }

        public Item AddNew(Type itemType, Guid newGuid, string name, ChangeContext localUserAdd)
        {
            throw new NotImplementedException();
        }
    }

    public class ItemTypeInfoCollection : ReadOnlyCollectionBase
    {
        public bool Contains(Type itemType)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class ItemTypeInfo : IComparable
    {
        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }

        public Type Type
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
    }

    public abstract class ItemPropertyDescriptor : PropertyDescriptor
    {
        protected ItemPropertyDescriptor(string name, Attribute[] attrs) : base(name, attrs)
        {
        }

        protected ItemPropertyDescriptor(MemberDescriptor descr) : base(descr)
        {
        }

        protected ItemPropertyDescriptor(MemberDescriptor descr, Attribute[] attrs) : base(descr, attrs)
        {
        }

        public bool TryGetValue(Item item, out object value)
        {
            throw new NotImplementedException();
        }
    }

}