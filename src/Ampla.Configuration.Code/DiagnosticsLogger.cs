﻿using System.Diagnostics;
using Ampla.Configuration;
using Citect.Common;

namespace Ampla.Code
{
    public class DiagnosticsLogger : ILogger
    {
        public void LogVerbose(string message)
        {
            Diagnostics.Write(TraceLevel.Verbose, message);
        }

        public void LogInformation(string message)
        {
            Diagnostics.Write(TraceLevel.Info, message);
        }

        public void LogWarning(string message)
        {
            Diagnostics.Write(TraceLevel.Warning, message);
        }
    }
}