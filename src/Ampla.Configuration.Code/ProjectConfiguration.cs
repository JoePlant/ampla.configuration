﻿using Ampla.Configuration;
using Citect.Ampla.Framework;

namespace Ampla.Code
{
    public static class ProjectConfiguration
    {
        private const string defaultDirectory = @"C:\Ampla\Configuration";
        private const string defaultFileMask = @"*.*";

        public static void ExecuteFile(Project project, string fileName)
        {
            Execute(project, defaultDirectory, fileName);
        }

        public static void Execute(Project project)
        {
            Execute(project, defaultDirectory, defaultFileMask);
        }

        public static void Execute(Project project, string directory, string fileMask)
        {
            ILogger logger = new DiagnosticsLogger();
            IProjectFactory projectFactory = new ProjectFactory(project);

            ConfigureProject configureProject = new ConfigureProject(projectFactory, logger);
            configureProject.Execute(directory, fileMask);
        }
    }


}