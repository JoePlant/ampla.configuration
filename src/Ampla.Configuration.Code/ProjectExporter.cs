﻿using Ampla.Configuration;
using Citect.Ampla.Framework;

namespace Ampla.Code
{
    public static class ProjectExporter
    {
        private const string defaultDirectory = @"C:\Ampla\Configuration\Export";

        public static void Execute(Project project)
        {
            Execute(project, defaultDirectory);
        }

        public static void Execute(Project project, string directory)
        {
            ILogger logger = new DiagnosticsLogger();
            IProjectFactory projectFactory = new ProjectFactory(project);

            ExportProject exportProject = new ExportProject(projectFactory, logger);
            exportProject.Execute(directory);
        }
    }
}