﻿using System;
using System.Collections.Generic;
using Ampla.Configuration;
using Citect.Ampla.Framework;

namespace Ampla.Code
{
    public class ProjectFactory : IProjectFactory
    {
        private readonly Project project;

        public ProjectFactory(Project project)
        {
            this.project = project;
        }

        public IProject GetProject()
        {
            return new ProjectWrapper(project, typeof(Item));
        }

        public IProject GetProject(string typeName)
        {
            Type itemType = GetItemType(typeName);
            return itemType != null ? new ProjectWrapper(project, itemType) : null;
        }

        public bool IsValidType(string type)
        {
            return GetItemType(type) != null;
        }

        public IList<string> GetItemTypes()
        {
            List<string> listTypes = new List<string>();
            ItemTypeInfoCollection itemTypes = project.ItemTypeInfos;
            foreach (ItemTypeInfo itemType in itemTypes)
            {
                listTypes.Add(itemType.Type.FullName);
            }
            return listTypes.AsReadOnly();
        }

        private Type GetItemType(string type)
        {
            ItemTypeInfoCollection itemTypes = project.ItemTypeInfos;

            foreach (ItemTypeInfo itemTypeInfo in itemTypes)
            {
                if (itemTypeInfo.Type.FullName == type)
                {
                    return itemTypeInfo.Type;
                }
            }

            return null;
        }
    }
}