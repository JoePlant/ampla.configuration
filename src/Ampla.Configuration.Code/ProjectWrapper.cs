﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Ampla.Configuration;
using Citect.Ampla.Data;
using Citect.Ampla.Framework;

namespace Ampla.Code
{
    public class ProjectWrapper : IProject
    {
        private readonly Type itemType;

        public ProjectWrapper(Project project, Type itemType)
        {
            Project = project;
            this.itemType = itemType;
        }

        protected Project Project { get; private set; }

        public bool Exists(string fullName)
        {
            Item item = Project[fullName];
            return item != null;
        }

        public object AddChild(string parent, string name)
        {
            Item item = null;
            if (parent == null)
            {
                item = Project.AddNew(itemType, Guid.NewGuid(), name, ChangeContext.LocalUserAdd);
                item.DisplayOrder = "5." + Project.RootItems.Count;
            }
            else
            {
                Item parentItem = Project[parent];
                if (parentItem != null)
                {
                    item = parentItem.AddNew(itemType, Guid.NewGuid(), name, ChangeContext.LocalUserAdd);
                    item.DisplayOrder = "5." + parentItem.Items.Count;
                }
            }
            return item;
        }

        public object FindItem(string fullName)
        {
            object item = Project[fullName];
            if (item != null && itemType.IsInstanceOfType(item))
            {
                return item;
            }
            return null;
        }

        public bool SaveRequired
        {
            get { return Project.HasChanges; }
        }

        public void Save()
        {
            Project.SaveChanges();
        }

        public bool IsNameValid(string name)
        {
            return Item.IsNameValid(name);
        }

        public T GetPropertyValue<T>(string fullName, string property)
        {
            Item item = Project[fullName];
            ItemPropertyDescriptor descriptor = item.FindPropertyDescriptor(property);

            object value;
            if (descriptor.TryGetValue(item, out value))
            {
                return (T)value;
            }
            return default(T);
        }

        public void SetPropertyValue<T>(string fullName, string property, T value)
        {
            Item item = Project[fullName];
            ItemPropertyDescriptor descriptor = item.FindPropertyDescriptor(property);
            descriptor.SetValue(item, value);
        }

        public PropertyDescriptorCollection GetProperties()
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(itemType);
            return properties;
        }

        public object[] FindAllItems()
        { 
            List<object> allItems = new List<object>();
            if (Project.ItemTypeInfos.Contains(itemType))
            {
                var itemsByType = Project.GetItemsByType(itemType);
                allItems.AddRange(itemsByType);
                return allItems.ToArray();
            }

            foreach (Item item in Project.AllItems)
            {
                if (itemType.IsInstanceOfType(item))
                {
                    allItems.Add(item);
                }
            }
            return allItems.ToArray();
        }

        public string GetFullName(object item)
        {
            Item i = item as Item;
            if (i != null)
            {
                return i.FullName;
            }

            throw new ArgumentException("Not an Item", "item");
        }

        public void Dispose()
        {
            Project = null;
        }
    }
}
