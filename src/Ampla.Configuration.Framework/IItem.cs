﻿using System.Collections.Generic;

namespace Ampla.Configuration.Framework
{
    public interface IItem
    {
        string Name { get; }
        IItemType ItemType { get; }
        IList<IItem> Items { get; }
        int TotalCount { get; }
        T GetProperty<T>(string propertyName);
        void SetProperty<T>(string propertyName, T newValue);
        IItem Add(IItemType itemType, string name);
        IItem Find(string name);
        string FullName { get; }
        IItem Parent { get; }
    }
}