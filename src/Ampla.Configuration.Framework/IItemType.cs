﻿using System.ComponentModel;

namespace Ampla.Configuration.Framework
{
    public interface IItemType
    {
        string Name { get; }
        PropertyDescriptor[] GetProperties();
        IItem Create(IItem parent, string name);
    }
}