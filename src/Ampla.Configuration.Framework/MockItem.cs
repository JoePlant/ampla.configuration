﻿using System.Collections.Generic;
using System.Linq;

namespace Ampla.Configuration.Framework
{
    public class MockItem : IItem
    {
        private readonly List<IItem> items = new List<IItem>();
        private readonly Dictionary<string, object> propertyValues = new Dictionary<string, object>();
        private readonly IItem parent;

        public MockItem(IItemType itemType, string name) : this(null, itemType, name)
        {
        }

        public MockItem(IItem parent, IItemType itemType, string name)
        {
            this.parent = parent;
            ItemType = itemType;
            Name = name;
        }

        public string Name { get; private set; }
        public IItemType ItemType { get; private set; }

        public T GetProperty<T>(string propertyName)
        {
            object value;
            return (T)(propertyValues.TryGetValue(propertyName, out value) ? value : default(T));
        }

        public void SetProperty<T>(string propertyName, T newValue)
        {
            propertyValues[propertyName] = newValue;
        }
    
        public IItem Add(IItemType itemType, string name)
        {
            IItem item = itemType.Create(this, name);
            items.Add(item);
            return item;
        }

        public IItem Find(string name)
        {
            return items.Find(i => i.Name == name);
        }

        public string FullName { get { return MockProject.GetFullName(this); } }
        public IItem Parent { get { return parent; } }

        public IList<IItem> Items
        {
            get { return items.AsReadOnly(); }
        }

        public int TotalCount
        {
            get
            {
                int count = 1;
                if (items.Count > 0)
                {
                    count += Items.Sum(mockItem => mockItem.TotalCount);
                }
                return count;
            }
        }
    }
}