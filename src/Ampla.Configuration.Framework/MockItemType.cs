﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Ampla.Configuration.Framework
{

    public class MockItemType : IItemType
    {
        public MockItemType(string itemTypeName) 
        {
            Name = itemTypeName;
        }

        public string Name { get; private set; }

        private readonly List<PropertyDescriptor> properties = new List<PropertyDescriptor>();

        public PropertyDescriptor[] GetProperties()
        {
            return properties.ToArray();
        }

        public MockItemType WithProperty<T>(string propertyName)
        {
            properties.Add(new MockPropertyDesciptor(propertyName, typeof(T)));
            return this;
        }

        public IItem Create(IItem parent, string name)
        {
            IItem item = new MockItem(parent, this, name);
            return item;
        }


        public class MockPropertyDesciptor : PropertyDescriptor 
        {
            private readonly Type propertyType;

            public MockPropertyDesciptor(string name, Type type) : base(name, new Attribute[0])
            {
                propertyType = type;
            }

            public override bool CanResetValue(object component)
            {
                return false;
            }

            public override object GetValue(object component)
            {
                MockItem item = component as MockItem;
                if (item != null)
                {
                    return Convert.ChangeType(item.GetProperty<object>(Name), propertyType);
                }
                return null;
            }

            public override void ResetValue(object component)
            {
                throw new NotImplementedException();
            }

            public override void SetValue(object component, object value)
            {
                MockItem item = component as MockItem;
                if (item != null)
                {
                    item.SetProperty(Name, value);
                }
            }

            public override bool ShouldSerializeValue(object component)
            {
                return true;
            }

            public override Type ComponentType
            {
                get { return typeof (MockItem); }
            }

            public override bool IsReadOnly
            {
                get { return false; }
            }

            public override Type PropertyType
            {
                get { return propertyType; }
            }
        }

    }
}