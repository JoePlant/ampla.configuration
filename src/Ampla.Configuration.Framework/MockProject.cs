﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ampla.Configuration.Framework
{
    public class MockProject 
    {
        private readonly List<IItem> rootItems = new List<IItem>();
        private int changes;

        private readonly List<IItemType> validTypes = new List<IItemType>(); 

        public bool Exists(string fullName)
        {
            if (string.IsNullOrEmpty(fullName))
            {
                return false;
            }

            IItem item = FindItem(fullName);
            return item != null;
        }

        private IItem FindItem(string fullName)
        {
            string[] parts = fullName.Split('.');
            string part = parts[0];
            IItem current = rootItems.Find(i => i.Name == part);
            for (int i = 1; i < parts.Length; i++)
            {
                string s = parts[i];
                if (current != null)
                {
                    current = current.Find(s);
                }
                if (current == null)
                {
                    return null;
                }
            }
            return current;
        }

        public IItem AddItem(string fullName)
        {
            IItem item = null;
            if (string.IsNullOrEmpty(fullName))
            {
                throw new ArgumentException("Empty name specified.");
            }

            IItemType firstItemType = validTypes.Count > 0 ? validTypes[0] : null;
            if (firstItemType == null)
            {
                throw new InvalidOperationException("No Types are registered.");
            }

            ItemFullName itemFullName = new ItemFullName(fullName);
            if (itemFullName.Parent == null)
            {
                item = firstItemType.Create(null, itemFullName.Name);
                rootItems.Add(item);
                changes++;
            }
            else
            {
                IItem parentItem = FindItem(itemFullName.Parent);
                if (parentItem != null)
                {
                    item = parentItem.Add(firstItemType,itemFullName.Name);
                    changes++;
                }
            }
            return item;
        }

        public int TotalCount
        {
            get
            {
                return rootItems.Aggregate(0, (current, mockItem) => current + mockItem.TotalCount);
            }
        }

        public bool SaveRequired { get { return changes > 0; }}

        public IList<string> GetItemTypes()
        {
            List<string> typeNames = validTypes.Select(itemType => itemType.Name).ToList();
            return typeNames.AsReadOnly();
        }

        public void Save()
        {
            changes = 0;
        }
        
        public bool IsNameValid(string name)
        {
            return (!string.IsNullOrEmpty(name) && !name.EndsWith(" ") && !name.StartsWith(" "));
        }

        public T GetPropertyValue<T>(string fullName, string property)
        {
            IItem item = FindItem(fullName);
            return item != null ? item.GetProperty<T>(property) : default(T);
        }

        public void SetPropertyValue<T>(string fullName, string property, T value)
        {
            IItem item = FindItem(fullName);
            if (item != null)
            {
                item.SetProperty(property, value);
            }
        }

        public bool ExecuteOnItem<TConfig>(string fullName, Func<IItem, TConfig, bool> func, TConfig config)
        {
            IItem item = FindItem(fullName);
            return item != null && func(item, config);
        }

        public IItem Find(string fullName)
        {
            return FindItem(fullName);
        }

        public void AddType(IItemType mockItemType)
        {
            if (mockItemType != null)
            {
                validTypes.Add(mockItemType);
            }
        }

        public IItemType GetItemType(string typeName)
        {
            return validTypes.FirstOrDefault(itemType => typeName == itemType.Name);
        }

        public static string GetFullName(IItem item)
        {
            string fullName = item.Name;
            IItem current = item.Parent;
            while (current != null)
            {
                fullName = current.Name + "." + fullName;
                current = current.Parent;
            }
            return fullName;
        }

        public IItem[] AllItems()
        {
            List<IItem> allItems = new List<IItem>();
            foreach (IItem item in rootItems)
            {
                addItemToList(allItems, item);
            }
            return allItems.ToArray();
        }

        private void addItemToList(List<IItem> allItems, IItem item)
        {
            allItems.Add(item);
            foreach (IItem child in item.Items)
            {
                addItemToList(allItems, child);
            }
        }
    }
}