﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Ampla.Configuration.Framework
{
    public class RealItem<TItem> : IItem where TItem : new()
    {
        private readonly List<IItem> items = new List<IItem>();
        private readonly TItem itemValue;
        private readonly RealItemType<TItem> realItemType;
        private readonly IItem parent;

        public RealItem(RealItemType<TItem> itemType, string name) : this(null, itemType, name)
        {
        }

        public RealItem(IItem parent, RealItemType<TItem> itemType, string name)
        {
            this.parent = parent;
            realItemType = itemType;
            Name = name;
            itemValue = new TItem();
        }

        public string Name { get; private set; }
        public IItemType ItemType { get { return realItemType; }}

        public TItem Value { get { return itemValue; } }

        public T GetProperty<T>(string propertyName)
        {
            PropertyDescriptor property = realItemType.GetPropertyDescriptor(propertyName);
            if (property == null)
            {
                throw new ArgumentException("No properties called: " + propertyName);
            }
            return (T)property.GetValue(this);
        }

        public void SetProperty<T>(string propertyName, T newValue)
        {
            PropertyDescriptor property = realItemType.GetPropertyDescriptor(propertyName);
            if (property == null)
            {
                throw new ArgumentException("No properties called: " + propertyName);
            }
            property.SetValue(this, newValue);
        }

        public IItem Add(IItemType itemType, string name)
        {
            IItem newItem = itemType.Create(this, name);
            items.Add(newItem);
            return newItem;
        }

        public IItem Find(string name)
        {
            return items.Find(i => i.Name == name);
        }

        public string FullName { get { return MockProject.GetFullName(this); } }

        public IItem Parent { get { return parent; }}

        public IList<IItem> Items
        {
            get { return items.AsReadOnly(); }
        }

        public int TotalCount
        {
            get
            {
                int count = 1;
                if (items.Count > 0)
                {
                    count += Items.Sum(mockItem => mockItem.TotalCount);
                }
                return count;
            }
        }

        public override string ToString()
        {
            return string.Format("{0} ({1})", FullName, typeof (TItem).FullName);
        }
    }
}