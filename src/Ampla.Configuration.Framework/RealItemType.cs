﻿using System;
using System.ComponentModel;

namespace Ampla.Configuration.Framework
{
    public class RealItemType<TItem> : IItemType where TItem : new()
    {
        private readonly PropertyDescriptorCollection properties;

        public RealItemType() 
        {
            Name = typeof(TItem).FullName;
            ItemType = typeof (TItem);
            properties = TypeDescriptor.GetProperties(typeof(TItem));
        }

        public string Name { get; private set; }

        public Type ItemType { get; private set; }

        public PropertyDescriptor GetPropertyDescriptor(string propertyName)
        {
            PropertyDescriptor valuePropertyDescriptor = properties[propertyName];
            if (valuePropertyDescriptor != null)
            {
                return new RealItemPropertyDesciptor<TItem>(valuePropertyDescriptor);
            }
            return null;
        }

        public PropertyDescriptor[] GetProperties()
        {
            PropertyDescriptor[] array = new PropertyDescriptor[properties.Count];
            for (int i = 0; i < properties.Count; i++)
            {
                array[i] = new RealItemPropertyDesciptor<TItem>(properties[i]);
            }

            return array;
        }

        public IItem Create(IItem parent, string name)
        {
            return new RealItem<TItem>(parent, this, name);
        }

        public class RealItemPropertyDesciptor<T>: PropertyDescriptor
        {
            private readonly PropertyDescriptor propertyDescriptor;

            public RealItemPropertyDesciptor(PropertyDescriptor propertyDescriptor)
                : base(propertyDescriptor)
            {
                this.propertyDescriptor = propertyDescriptor;
            }

            public override bool CanResetValue(object component)
            {
                return propertyDescriptor.CanResetValue(component);
            }

            public override object GetValue(object component)
            {
                RealItem<TItem> item = component as RealItem<TItem>;
                if (item != null)
                {
                    return propertyDescriptor.GetValue(item.Value);
                }
                return null;
            }

            public override void ResetValue(object component)
            {
                propertyDescriptor.ResetValue(component);
            }

            public override void SetValue(object component, object value)
            {
                RealItem<TItem> item = component as RealItem<TItem>;
                if (item != null)
                {
                    propertyDescriptor.SetValue(item.Value, value);
                }
            }

            public override bool ShouldSerializeValue(object component)
            {
                return propertyDescriptor.ShouldSerializeValue(component);
            }

            public override Type ComponentType
            {
                get { return propertyDescriptor.ComponentType; }
            }

            public override bool IsReadOnly
            {
                get { return propertyDescriptor.IsReadOnly; }
            }

            public override Type PropertyType
            {
                get { return propertyDescriptor.PropertyType; }
            }
        }
    }
}