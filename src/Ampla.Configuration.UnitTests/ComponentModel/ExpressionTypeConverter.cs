﻿using System;
using System.ComponentModel;
using System.Globalization;
using Ampla.Configuration.Items;

namespace Ampla.Configuration.ComponentModel
{
    public class ExpressionTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (context != null) && sourceType == typeof (string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value != null)
            {
                if (context != null && context.PropertyDescriptor != null)
                {
                    Expression expression = (Expression) context.PropertyDescriptor.GetValue(context.Instance) ??
                                            new Expression();

                    expression.Text = (string) value;
                    return expression;
                }
            }
            return null;
        }
    }
}