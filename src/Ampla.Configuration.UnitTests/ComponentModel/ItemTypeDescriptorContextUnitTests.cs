﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Ampla.Configuration.Framework;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration.ComponentModel
{
    [TestFixture]
    public class ItemTypeDescriptorContextUnitTests : ProjectTestFixture
    {
        private readonly RealItemType<Folder> itemType = new RealItemType<Folder>();
 
        protected override List<IItemType> GetItemTypes()
        {
            return new List<IItemType> { itemType};
        }


        [Test]
        public void PropertiesAreSet()
        {
            IItem item = Project.AddItem("Enterprise");

            Assert.That(item, Is.Not.Null);
            Assert.That(item.ItemType, Is.EqualTo(itemType));

            PropertyDescriptor property = itemType.GetProperties()[0];
            Assert.That(property, Is.Not.Null);

            ItemTypeDescriptorContext context = new ItemTypeDescriptorContext(item, property);

            Assert.That(context.Container, Is.Null);
            Assert.That(context.Instance, Is.EqualTo(item));
            Assert.That(context.PropertyDescriptor, Is.EqualTo(property));
        }

    }
}