﻿using System.Collections.Generic;
using Ampla.Configuration.Framework;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration
{
    [TestFixture]
    public class ConfigureProjectUnitTests : ProjectTestFixture
    {
        protected override List<IItemType> GetItemTypes()
        {
            return new List<IItemType>
                {
                    new RealItemType<Folder>(),
                    new RealItemType<CauseCode>(),
                    new RealItemType<ReportingPoint>(),
                    new RealItemType<Field>()
                };
        }

        [Test]
        public void Execute()
        {
            Project.AddItem("System Configuration");
            Project.AddItem("System Configuration.Lookup Lists");
            Project.AddItem("System Configuration.Lookup Lists.Cause Codes");

            AssertItemExists("System Configuration.Lookup Lists.Cause Codes");

            ConfigureProject configureProject = new ConfigureProject(Factory, Logger);
            configureProject.Execute(@".\Resources\Add", "*.*");
            AssertWarningCount(0);
        }
    }
}