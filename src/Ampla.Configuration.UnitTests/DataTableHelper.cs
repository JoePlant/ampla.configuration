﻿using System.Data;
using System.Linq;
using NUnit.Framework;

namespace Ampla.Configuration
{
    public static class DataTableHelper
    {
        public static void AssertTable(this DataTable dataTable, string[] columns, object[][] rows)
        {
            string expectedColumns = columns.Length.ToString();
            if (columns.Length > 0)
            {
                expectedColumns += "|" + string.Join("|", columns);
            }

            string actualColumns = dataTable.Columns.Cast<DataColumn>()
                                            .Aggregate("" + dataTable.Columns.Count,
                                                       (current, column) => current + "|" + (column.ColumnName));
            Assert.That(actualColumns, Is.EqualTo(expectedColumns), "Columns");
            Assert.That(dataTable.Rows.Count, Is.EqualTo(rows.Length), "Rows.Count");

            int rowCount = dataTable.Rows.Count;
            for (int i = 0; i < rowCount; i++)
            {
                object[] actualRow = dataTable.Rows[i].ItemArray;
                object[] expectedRow = rows[i];
                Assert.That(actualRow.Length, Is.EqualTo(expectedRow.Length), "Rows[{0}].Columns.Length", i);
                Assert.That(actualRow, Is.EqualTo(expectedRow), "Rows[{0}].Values", i);
            }
        }

        public static void AssertRow(DataTable dataTable, int row, params object[] expected)
        {
            DataRow dataRow = dataTable.Rows[row];
            Assert.That(dataRow.Table.Columns.Count, Is.EqualTo(expected.Length), "Number of columns are different");
            object[] actualValues = dataRow.ItemArray;
            Assert.That(actualValues, Is.EqualTo(expected), "Rows[{0}]", row);
        } 
    }
}