﻿using System.Collections.Generic;
using System.IO;
using Ampla.Configuration.Framework;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration
{
    [TestFixture]
    public class ExportProjectUnitTests : ProjectTestFixture
    {
        private TempDirectory tempDirectory;
        private readonly RealItemType<CauseCode> causeCodeType = new RealItemType<CauseCode>();

        protected override void OnFixtureSetUp()
        {
            base.OnFixtureSetUp();
            tempDirectory = TempDirectory.ForUnitTests(this, "txt");
        }

        protected override List<IItemType> GetItemTypes()
        {
            return new List<IItemType>
                {
                    new RealItemType<Folder>(),
                    causeCodeType,
                    new RealItemType<ReportingPoint>(),
                    new RealItemType<Field>()
                };
        }

        [Test]
        public void ProjectWithItems()
        {
            Project.AddItem("System Configuration");
            Project.AddItem("System Configuration.Lookup Lists");
            Project.AddItem("System Configuration.Lookup Lists.Cause Codes").Add(causeCodeType, "Broken");

            Project.SetPropertyValue("System Configuration.Lookup Lists.Cause Codes.Broken", "CauseCodeId", 10);

            ExportProject configureProject = new ExportProject(Factory, Logger);

            string directory = tempDirectory.GetSpecificDirectory("ProjectWithItems");
            configureProject.Execute(directory);
            AssertWarningCount(0);
        }

        [Test]
        public void EmptyProject()
        {
            ExportProject configureProject = new ExportProject(Factory, Logger);

            string directory = tempDirectory.GetSpecificDirectory("EmptyProject");
            configureProject.Execute(directory);
            AssertWarningCount(0);

            Assert.That(Directory.EnumerateFiles(directory), Is.Empty);
        }

        [Test]
        public void EmptyProjectWithAllTypes()
        {
            ExportProject configureProject = new ExportProject(Factory, Logger) {SkipTypesNotUsed = false};

            string directory = tempDirectory.GetSpecificDirectory("EmptyProjectWithAllTypes");
            configureProject.Execute(directory);
            AssertWarningCount(0);

            Assert.That(Directory.EnumerateFiles(directory), Is.Not.Empty);
        }
    }
}