﻿using Ampla.Configuration.ComponentModel;
using NUnit.Framework;

namespace Ampla.Configuration.Framework
{
    [TestFixture]
    public class ItemFullNameUnitTests : TestFixture
    {
        [Test]
        public void Parent()
        {
            ItemFullName item = new ItemFullName("Enterprise.Site.Area");
            Assert.That(item.Parent, Is.EqualTo("Enterprise.Site"));

            ItemFullName parent = new ItemFullName(item.Parent);
            Assert.That(parent.Parent, Is.EqualTo("Enterprise"));

            ItemFullName grandParent = new ItemFullName(parent.Parent);
            Assert.That(grandParent.Parent, Is.EqualTo(null));
        }

        [Test]
        public void Name()
        {
            ItemFullName item = new ItemFullName("Enterprise.Site.Area");
            Assert.That(item.Name, Is.EqualTo("Area"));

            ItemFullName parent = new ItemFullName(item.Parent);
            Assert.That(parent.Name, Is.EqualTo("Site"));

            ItemFullName grandParent = new ItemFullName(parent.Parent);
            Assert.That(grandParent.Name, Is.EqualTo("Enterprise"));
        }

        [Test]
        public void FullName()
        {
            ItemFullName item = new ItemFullName("Enterprise.Site.Area");
            Assert.That(item.FullName, Is.EqualTo("Enterprise.Site.Area"));

            ItemFullName parent = new ItemFullName(item.Parent);
            Assert.That(parent.FullName, Is.EqualTo("Enterprise.Site"));

            ItemFullName grandParent = new ItemFullName(parent.Parent);
            Assert.That(grandParent.FullName, Is.EqualTo("Enterprise"));
        }
        
        [Test]
        public void ParentAndName()
        {
            ItemFullName item = new ItemFullName("Enterprise.Site", "Area");
            Assert.That(item.Parent, Is.EqualTo("Enterprise.Site"));
            Assert.That(item.Name, Is.EqualTo("Area"));
            Assert.That(item.FullName, Is.EqualTo("Enterprise.Site.Area"));
        }

        [Test]
        public void EmptyParentAndName()
        {
            ItemFullName item = new ItemFullName("", "Enterprise");
            Assert.That(item.Parent, Is.EqualTo(null));
            Assert.That(item.Name, Is.EqualTo("Enterprise"));
            Assert.That(item.FullName, Is.EqualTo("Enterprise"));
        }

        [Test]
        public void NullParentAndName()
        {
            ItemFullName item = new ItemFullName(null, "Enterprise");
            Assert.That(item.Parent, Is.EqualTo(null));
            Assert.That(item.Name, Is.EqualTo("Enterprise"));
            Assert.That(item.FullName, Is.EqualTo("Enterprise"));
        }

        [Test]
        public void FullNameToString()
        {
            ItemFullName item = new ItemFullName("Enterprise.Site.Area");
            Assert.That(item.ToString(), Is.EqualTo("{Enterprise.Site.Area}"));
        }

        [Test]
        public void ParentNameToString()
        {
            ItemFullName item = new ItemFullName("Enterprise.Site","Area");
            Assert.That(item.ToString(), Is.EqualTo("{Enterprise.Site}.{Area}"));
        }

        [Test]
        public void NameToString()
        {
            ItemFullName item = new ItemFullName(null, "Enterprise");
            Assert.That(item.ToString(), Is.EqualTo("{Enterprise}"));

            item = new ItemFullName(string.Empty, "Enterprise");
            Assert.That(item.ToString(), Is.EqualTo("{Enterprise}"));
        }
    }
}