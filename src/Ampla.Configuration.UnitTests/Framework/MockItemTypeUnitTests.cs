﻿using System.ComponentModel;
using NUnit.Framework;

namespace Ampla.Configuration.Framework
{
    [TestFixture]
    public class MockItemTypeUnitTests : TestFixture
    {
        [Test]
        public void NoProperties()
        {
            MockItemType itemType = new MockItemType("Folder");
            Assert.That(itemType.Name, Is.EqualTo("Folder"));
            Assert.That(itemType.GetProperties(), Is.Empty);
        }

        [Test]
        public void GetSetPropertyViaType()
        {
            MockItemType itemType = new MockItemType("Folder").WithProperty<string>("EquipmentId");
            Assert.That(itemType.Name, Is.EqualTo("Folder"));
            PropertyDescriptor[] properties = itemType.GetProperties();
            Assert.That(properties, Is.Not.Empty);
            Assert.That(properties.Length, Is.EqualTo(1));
            PropertyDescriptor equipmentId = properties[0];
            MockItem item = new MockItem(itemType, "Enterprise");
            
            Assert.That(equipmentId.GetValue(item), Is.Null);

            equipmentId.SetValue(item, "Ent");
            Assert.That(equipmentId.GetValue(item), Is.EqualTo("Ent"));

            Assert.That(item.GetProperty<string>("EquipmentId"), Is.EqualTo("Ent"));
        }

        [Test]
        public void WithProperty()
        {
            MockItemType item = new MockItemType("Folder");
            Assert.That(item.Name, Is.EqualTo("Folder"));

            Assert.That(item.GetProperties(), Is.Empty);
            Assert.That(item.WithProperty<string>("EquipmentID"), Is.EqualTo(item));

            Assert.That(item.GetProperties(), Is.Not.Empty);
            PropertyDescriptor equipmentId = item.GetProperties()[0];
            Assert.That(equipmentId.PropertyType, Is.EqualTo(typeof(string)));

            Assert.That(item.WithProperty<int>("DisplayOrder"), Is.EqualTo(item));
            Assert.That(item.GetProperties().Length, Is.EqualTo(2));

            PropertyDescriptorCollection properties = new PropertyDescriptorCollection(item.GetProperties());
            Assert.That(properties["DisplayOrder"], Is.Not.Null);
            Assert.That(properties["DisplayOrder"].PropertyType, Is.EqualTo(typeof(int)));

        }

    }
}