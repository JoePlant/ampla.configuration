﻿using System.Collections.Generic;
using NUnit.Framework;

namespace Ampla.Configuration.Framework
{
    [TestFixture]
    public class MockItemUnitTests : ProjectTestFixture
    {
        private readonly IItemType enterpriseType = new MockItemType("EnterpriseType");
        private readonly IItemType siteType = new MockItemType("SiteType");
        protected override List<IItemType> GetItemTypes()
        {
            return new List<IItemType> {enterpriseType, siteType};

        }

        [Test]
        public void NameAndType()
        {
            MockItem item = new MockItem(enterpriseType, "Enterprise");
            Assert.That(item.Name, Is.EqualTo("Enterprise"));
            Assert.That(item.ItemType, Is.EqualTo(enterpriseType));
            Assert.That(item.Parent, Is.Null);
            Assert.That(item.FullName, Is.EqualTo("Enterprise"));

        }

        [Test]
        public void AddNameAndType()
        {
            MockItem enterprise = new MockItem(enterpriseType, "Enterprise");
            enterprise.Add(siteType, "Site");
            Assert.That(enterprise.Name, Is.EqualTo("Enterprise"));
            Assert.That(enterprise.ItemType, Is.EqualTo(enterpriseType));
            IItem site = enterprise.Find("Site");
            Assert.That(site.Name, Is.EqualTo("Site"));
            Assert.That(site.ItemType, Is.EqualTo(siteType));

            Assert.That(site.Parent, Is.EqualTo(enterprise));
            Assert.That(enterprise.Parent, Is.EqualTo(null));

            Assert.That(enterprise.FullName, Is.EqualTo("Enterprise"));
            Assert.That(site.FullName, Is.EqualTo("Enterprise.Site"));
        }

        [Test]
        public void Find()
        {
            MockItem enterprise = new MockItem(enterpriseType, "Enterprise");
            IItem site = enterprise.Add(siteType, "Site");
            Assert.That(enterprise.Name, Is.EqualTo("Enterprise"));
            Assert.That(enterprise.ItemType, Is.EqualTo(enterpriseType));
            Assert.That(site, Is.Not.Null);
            Assert.That(site.Name, Is.EqualTo("Site"));

            Assert.That(enterprise.Find("site"), Is.Null);
            Assert.That(enterprise.Find("SITE"), Is.Null);
            Assert.That(enterprise.Find("sItE"), Is.Null);
            Assert.That(enterprise.Find("Site"), Is.Not.Null);
            
            Assert.That(site.Find("Area"), Is.Null);
        }

    }
}