﻿using System.Collections.Generic;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration.Framework
{
    [TestFixture]
    public class MockProjectUnitTests : ProjectTestFixture
    {
        private readonly RealItemType<Folder> folderType = new RealItemType<Folder>();

        protected override List<IItemType> GetItemTypes()
        {
            return new List<IItemType> { folderType };
        }

        [Test]
        public void EmptyProject()
        {
            Assert.That(Project.TotalCount, Is.EqualTo(0));
        }

        [Test]
        public void Level1()
        {
            AssertItemNotExists("Enterprise");
            AssertItemNotExists("Other");

            Assert.That(Project.TotalCount, Is.EqualTo(0));

            Project.AddItem("Enterprise");

            Assert.That(Project.TotalCount, Is.EqualTo(1));
            AssertItemExists("Enterprise");

            Project.AddItem("Other");
            Assert.That(Project.TotalCount, Is.EqualTo(2));
            AssertItemExists("Other");
        }

        [Test]
        public void Level2()
        {
            AssertItemNotExists("Enterprise");
            AssertItemNotExists("Enterprise.Site 1");
            AssertItemNotExists("Enterprise.Site 2");
            Assert.That(Project.TotalCount, Is.EqualTo(0));

            Project.AddItem("Enterprise");

            Assert.That(Project.TotalCount, Is.EqualTo(1));

            Project.AddItem("Enterprise.Site 1");
            Assert.That(Project.TotalCount, Is.EqualTo(2));

            Project.AddItem("Enterprise.Site 2");
            Assert.That(Project.TotalCount, Is.EqualTo(3));

            AssertItemExists("Enterprise");
            AssertItemExists("Enterprise.Site 1");
            AssertItemExists("Enterprise.Site 2");
        }

        [Test]
        public void Level3()
        {
            AssertItemNotExists("Enterprise");
            AssertItemNotExists("Enterprise.Site 1");
            AssertItemNotExists("Enterprise.Site 1.Area A");
            AssertItemNotExists("Enterprise.Site 1.Area B");
            AssertItemNotExists("Enterprise.Site 2");

            Assert.That(Project.TotalCount, Is.EqualTo(0));

            Project.AddItem("Enterprise");

            Assert.That(Project.TotalCount, Is.EqualTo(1));

            Project.AddItem("Enterprise.Site 1");
            Assert.That(Project.TotalCount, Is.EqualTo(2));

            Project.AddItem("Enterprise.Site 2");
            Assert.That(Project.TotalCount, Is.EqualTo(3));

            Project.AddItem("Enterprise.Site 1.Area A");
            Assert.That(Project.TotalCount, Is.EqualTo(4));

            Project.AddItem("Enterprise.Site 1.Area B");
            Assert.That(Project.TotalCount, Is.EqualTo(5));

            AssertItemExists("Enterprise");
            AssertItemExists("Enterprise.Site 1");
            AssertItemExists("Enterprise.Site 1.Area A");
            AssertItemExists("Enterprise.Site 1.Area B");
            AssertItemExists("Enterprise.Site 2");
        }

        [Test]
        public void Save()
        {
            Assert.That(Project.TotalCount, Is.EqualTo(0));

            Project.AddItem("Enterprise");

            Assert.That(Project.TotalCount, Is.EqualTo(1));
            Assert.That(Project.SaveRequired, Is.EqualTo(true));

            Project.AddItem("Enterprise.Site 1");
            Assert.That(Project.TotalCount, Is.EqualTo(2));
            Assert.That(Project.SaveRequired, Is.EqualTo(true));

            Project.Save();

            Assert.That(Project.TotalCount, Is.EqualTo(2));
            Assert.That(Project.SaveRequired, Is.EqualTo(false));

            Project.AddItem("Enterprise.Site 2");
            Assert.That(Project.TotalCount, Is.EqualTo(3));
            Assert.That(Project.SaveRequired, Is.EqualTo(true));

            Project.AddItem("Enterprise.Site 1.Area A");
            Assert.That(Project.TotalCount, Is.EqualTo(4));
            Assert.That(Project.SaveRequired, Is.EqualTo(true));

            Project.AddItem("Enterprise.Site 1.Area B");
            Assert.That(Project.TotalCount, Is.EqualTo(5));
            Assert.That(Project.SaveRequired, Is.EqualTo(true));
        }

        [Test]
        public void IsNameValid()
        {
            Assert.That(Project.IsNameValid(null), Is.EqualTo(false));
            Assert.That(Project.IsNameValid(""), Is.EqualTo(false));
            Assert.That(Project.IsNameValid("Enterprise"), Is.EqualTo(true));
            Assert.That(Project.IsNameValid("Enterprise "), Is.EqualTo(false));
            Assert.That(Project.IsNameValid(" Enterprise"), Is.EqualTo(false));
        }
        
        [Test]
        public void ItemTypesAreRegistered()
        {
            Assert.That(Project.GetItemType(typeof(Folder).FullName), Is.Not.Null);
            Assert.That(Project.GetItemType("Classification"), Is.Null);

            Project.AddType(new MockItemType("Classification").WithProperty<int>("ClassificationID"));
            Assert.That(Project.GetItemType("Classification"), Is.Not.Null);

            object item = Project.AddItem("Enterprise");

            Assert.That(item, Is.Not.Null);
        }

        [Test]
        public void GetFullName()
        {
            IItem enterprise = Project.AddItem("Enterprise");

            IItem site = enterprise.Add(folderType, "Site");
            IItem area = site.Add(folderType, "Area");
           
 
            Assert.That(ProjectView.GetFullName(enterprise), Is.EqualTo("Enterprise"));
            Assert.That(ProjectView.GetFullName(site), Is.EqualTo("Enterprise.Site"));
            Assert.That(ProjectView.GetFullName(area), Is.EqualTo("Enterprise.Site.Area"));
        }

        [Test]
        public void GetAllItems()
        {
            Assert.That(Project.AllItems(), Is.Empty);

            Project.AddItem("Enterprise");
            Assert.That(Project.AllItems().Length, Is.EqualTo(1));

            Project.AddItem("Enterprise.Site 1");
            Assert.That(Project.TotalCount, Is.EqualTo(2));
            Assert.That(Project.AllItems().Length, Is.EqualTo(2));

            Project.AddItem("Enterprise.Site 2");
            Assert.That(Project.TotalCount, Is.EqualTo(3));
            Assert.That(Project.AllItems().Length, Is.EqualTo(3));

            Project.AddItem("Enterprise.Site 1.Area A");
            Assert.That(Project.TotalCount, Is.EqualTo(4));
            Assert.That(Project.AllItems().Length, Is.EqualTo(4));


            Project.AddItem("Enterprise.Site 1.Area B");
            Assert.That(Project.TotalCount, Is.EqualTo(5));
            Assert.That(Project.AllItems().Length, Is.EqualTo(5));
        }

        [Test]
        public void GetItemType()
        {
            Assert.That(Project.GetItemType(typeof (Folder).FullName), Is.EqualTo(folderType));
            Assert.That(Factory.IsValidType(typeof (Folder).FullName), Is.EqualTo(true));

            Assert.That(Project.GetItemType(folderType.GetType().Name), Is.Null);
            Assert.That(Project.GetItemType(typeof(ReportingPoint).FullName), Is.Null);
        }

        [Test]
        public void TestGetItemTypes()
        {
            List<string> actual = new List<string>(Project.GetItemTypes());
            List<string> expected = new List<string> { typeof(Folder).FullName };
            Assert.That(actual, Is.EqualTo(expected));

            Project.AddType(new RealItemType<ReportingPoint>());
        }

        [Test]
        public void TestGetItemTypesWithExtra()
        {
            Project.AddType(new RealItemType<ReportingPoint>());

            List<string> actual = new List<string>(Project.GetItemTypes());
            List<string> expected = new List<string> { typeof(Folder).FullName, typeof(ReportingPoint).FullName };
            Assert.That(actual, Is.EqualTo(expected));
        }


    }
}