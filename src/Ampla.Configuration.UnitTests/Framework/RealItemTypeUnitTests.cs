﻿using System.ComponentModel;
using NUnit.Framework;

namespace Ampla.Configuration.Framework
{
    [TestFixture]
    public class RealItemTypeUnitTests : TestFixture
    {
        public class TestFolder
        {
            public string EquipmentId { get; set; }
        }

        [Test]
        public void NoProperties()
        {
            RealItemType<object> itemType = new RealItemType<object>();
            Assert.That(itemType.Name, Is.EqualTo(typeof(object).FullName));
            Assert.That(itemType.GetProperties(), Is.Empty);
        }

        [Test]
        public void GetSetPropertyViaType()
        {
            RealItemType<TestFolder> itemType = new RealItemType<TestFolder>();
            Assert.That(itemType.Name, Is.EqualTo("Ampla.Configuration.Framework.RealItemTypeUnitTests+TestFolder"));
            PropertyDescriptor[] properties = itemType.GetProperties();
            Assert.That(properties, Is.Not.Empty);
            Assert.That(properties.Length, Is.EqualTo(1));
            PropertyDescriptor equipmentId = properties[0];

            RealItem<TestFolder> item = new RealItem<TestFolder>(itemType, "Enterprise");
            Assert.That(item.Value.EquipmentId, Is.EqualTo(null));
            
            Assert.That(equipmentId.GetValue(item), Is.Null);

            equipmentId.SetValue(item, "Ent");
            Assert.That(equipmentId.GetValue(item), Is.EqualTo("Ent"));

            Assert.That(item.GetProperty<string>("EquipmentId"), Is.EqualTo("Ent"));

            Assert.That(item.Value.EquipmentId, Is.EqualTo("Ent"));
        }

        [Test]
        public void WithProperty()
        {
            MockItemType item = new MockItemType("Folder");
            Assert.That(item.Name, Is.EqualTo("Folder"));

            Assert.That(item.GetProperties(), Is.Empty);
            Assert.That(item.WithProperty<string>("EquipmentID"), Is.EqualTo(item));

            Assert.That(item.GetProperties(), Is.Not.Empty);
            PropertyDescriptor equipmentId = item.GetProperties()[0];
            Assert.That(equipmentId.PropertyType, Is.EqualTo(typeof(string)));

            Assert.That(item.WithProperty<int>("DisplayOrder"), Is.EqualTo(item));
            Assert.That(item.GetProperties().Length, Is.EqualTo(2));

            PropertyDescriptorCollection properties = new PropertyDescriptorCollection(item.GetProperties());
            Assert.That(properties["DisplayOrder"], Is.Not.Null);
            Assert.That(properties["DisplayOrder"].PropertyType, Is.EqualTo(typeof(int)));

        }
    }
}