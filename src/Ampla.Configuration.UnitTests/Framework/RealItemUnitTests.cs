﻿using System.Collections.Generic;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration.Framework
{
    [TestFixture]
    public class RealItemUnitTests : ProjectTestFixture
    {
        private readonly RealItemType<Folder> folderType = new RealItemType<Folder>();
        private readonly IItemType pointType = new RealItemType<ReportingPoint>();
        protected override List<IItemType> GetItemTypes()
        {
            return new List<IItemType> { folderType, pointType };

        }

        [Test]
        public void NameAndType()
        {
            RealItem<Folder> item = new RealItem<Folder>(folderType,"Enterprise");
            Assert.That(item.Name, Is.EqualTo("Enterprise"));
            Assert.That(item.ItemType, Is.EqualTo(folderType));
            Assert.That(item.Parent, Is.Null);
            Assert.That(item.FullName, Is.EqualTo("Enterprise"));
        }

        [Test]
        public void AddNameAndType()
        {
            RealItem<Folder> enterprise = new RealItem<Folder>(folderType, "Enterprise");
            IItem point = enterprise.Add(pointType, "Point");
            Assert.That(enterprise.Name, Is.EqualTo("Enterprise"));
            Assert.That(enterprise.ItemType, Is.EqualTo(folderType));
            IItem site = enterprise.Find("Point");
            Assert.That(site.Name, Is.EqualTo("Point"));
            Assert.That(site.ItemType, Is.EqualTo(pointType));

            Assert.That(site.Parent, Is.EqualTo(enterprise));
            Assert.That(enterprise.Parent, Is.EqualTo(null));

            Assert.That(enterprise.FullName, Is.EqualTo("Enterprise"));
            Assert.That(site.FullName, Is.EqualTo("Enterprise.Point"));

            Assert.That(enterprise.ToString(), Is.EqualTo("Enterprise (Ampla.Configuration.Items.Folder)"));
            Assert.That(point.ToString(), Is.EqualTo("Enterprise.Point (Ampla.Configuration.Items.ReportingPoint)"));
        }

        [Test]
        public void Find()
        {
            MockItem enterprise = new MockItem(folderType, "Enterprise");
            IItem point = enterprise.Add(pointType, "Point");
            Assert.That(enterprise.Name, Is.EqualTo("Enterprise"));
            Assert.That(enterprise.ItemType, Is.EqualTo(folderType));
            Assert.That(point, Is.Not.Null);
            Assert.That(point.Name, Is.EqualTo("Point"));

            Assert.That(enterprise.Find("point"), Is.Null);
            Assert.That(enterprise.Find("POINT"), Is.Null);
            Assert.That(enterprise.Find("pOiNt"), Is.Null);
            Assert.That(enterprise.Find("Point"), Is.Not.Null);

            Assert.That(point.Find("Area"), Is.Null);
        }
    }
}