﻿using System.ComponentModel;
using Ampla.Configuration.ComponentModel;

namespace Ampla.Configuration.Items
{
    [TypeConverter(typeof(ExpressionTypeConverter))]
    public class Expression
    {
        public string Text { get; set; }
        
        protected bool Equals(Expression other)
        {
            return other != null && string.Equals(Text, other.Text);
        }

        public override int GetHashCode()
        {
            return (Text != null ? Text.GetHashCode() : 0);
        }
        
        public override string ToString()
        {
            return string.Format("Expression (Text = '{0}')", Text);
        }

        public override bool Equals(object obj)
        {
            Expression other = obj as Expression;
            return Equals(other);
        }

    }

 
}