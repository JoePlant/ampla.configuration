﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Ampla.Configuration.ComponentModel;
using Ampla.Configuration.Framework;
using NUnit.Framework;

namespace Ampla.Configuration.Items
{
    [TestFixture]
    public class ExpressionUnitTests : ProjectTestFixture
    {
        private readonly RealItemType<Field> itemType = new RealItemType<Field>();

        protected override List<IItemType> GetItemTypes()
        {
            return new List<IItemType> { itemType };
        }

        [Test]
        public void TypeConverter()
        {
            RealItem<Field> item = (RealItem<Field>) Project.AddItem("Field");
            Assert.That(item, Is.Not.Null);
            const string value = "10";

            
            PropertyDescriptor descriptor = itemType.GetPropertyDescriptor("Expression");

            TypeConverter typeConverter = new ExpressionTypeConverter();

            ItemTypeDescriptorContext context = new ItemTypeDescriptorContext(item, descriptor);

            Assert.That(typeConverter.ConvertFromInvariantString(value), Is.Null);
            Expression expression = (Expression) typeConverter.ConvertFromInvariantString(context, value);
            Assert.That(expression, Is.Not.Null);
            if (expression != null)
            {
                Assert.That(expression.Text, Is.EqualTo(value));
            }

        }


    }
}