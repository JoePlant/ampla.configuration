﻿using Ampla.Configuration.Framework;

namespace Ampla.Configuration.Items
{
    public class Field
    {
        public Expression Expression { get; set; }
        
        public string Description { get; set; }

        public string[] Category { get; set; }

        public string[] SubCategory { get; set; }
    }
}