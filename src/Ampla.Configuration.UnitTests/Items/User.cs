﻿namespace Ampla.Configuration.Items
{
    public enum UserAuthenticationMode { WindowsIntegrated, Simple, Any }

    public class User
    {
        public string Password { get; set; }
        public UserAuthenticationMode AuthenticationMode { get; set; }
    }
}