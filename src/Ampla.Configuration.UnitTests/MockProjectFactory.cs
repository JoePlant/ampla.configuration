﻿
using System.Collections.Generic;
using Ampla.Configuration.Framework;

namespace Ampla.Configuration
{
    public class MockProjectFactory : IProjectFactory
    {
        private readonly MockProject project;

        public MockProjectFactory(MockProject project)
        {
            this.project = project;
        }

        public IProject GetProject()
        {
            return new MockProjectView(project, null);
        }


        public IProject GetProject(string typeName)
        {
            IItemType itemType = project.GetItemType(typeName);
            
            if (IsValidType(typeName))
            {
                return new MockProjectView(project, itemType);
            }
            return null;
        }

        public bool IsValidType(string type)
        {
            return project.GetItemType(type) != null;
        }

        public IList<string> GetItemTypes()
        {
            return project.GetItemTypes();
        }
    }
}