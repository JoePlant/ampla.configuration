﻿using System;
using Ampla.Configuration.Framework;
using NUnit.Framework;

namespace Ampla.Configuration
{
    [TestFixture]
    public class MockProjectFactoryUnitTests : TestFixture
    {
        [Test]
        public void AddItem()
        {
            MockProject project = new MockProject();
            project.AddType(new MockItemType("Folder"));

            Assert.That(project.TotalCount, Is.EqualTo(0));

            project.AddItem("Enterprise");
            project.AddItem("Enterprise.Site");
            project.AddItem("Enterprise.Site.Area");

            Assert.That(project.TotalCount, Is.EqualTo(3));
        }
    }
}