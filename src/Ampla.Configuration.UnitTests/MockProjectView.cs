﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Ampla.Configuration.Framework;

namespace Ampla.Configuration
{
    public class MockProjectView : IProject
    {
        private readonly IItemType itemType;

        public MockProjectView(MockProject project, IItemType itemType)
        {
            Project = project;
            this.itemType = itemType;
        }

        protected MockProject Project { get; private set; }

        public bool Exists(string fullName)
        {
            return Project.Exists(fullName);
        }

        public object AddChild(string parent, string name)
        {
            if (itemType == null)
            {
                throw new InvalidOperationException("Default itemType is null.");
            }
            if (parent == null)
            {
                return Project.AddItem(name);
            }
            IItem item = Project.Find(parent);
            return item.Add(itemType, name);
        }

        public object FindItem(string fullName)
        {
            IItem item = Project.Find(fullName);
            if (item != null)
            {
                if (itemType == null)
                {
                    return item;
                }
                if (itemType == item.ItemType)
                {
                    return item;
                }
            }
            return null;
        }

        public bool SaveRequired
        {
            get { return Project.SaveRequired; }
        }

        public void Save()
        {
            Project.Save();
        }

        public bool IsNameValid(string name)
        {
            return Project.IsNameValid(name);
        }

        public T GetPropertyValue<T>(string fullName, string property)
        {
            IItem item = Project.Find(fullName);
            return item != null ? item.GetProperty<T>(property) : default(T);
        }

        public void SetPropertyValue<T>(string fullName, string property, T value)
        {
            IItem item = Project.Find(fullName);
            if (item != null)
            {
                item.SetProperty(property, value);
            }
        }

        public PropertyDescriptorCollection GetProperties()
        {
            if (itemType != null)
            {
                return new PropertyDescriptorCollection(itemType.GetProperties());
            }
            return new PropertyDescriptorCollection(new PropertyDescriptor[0]);
        }

        public object[] FindAllItems()
        {
            IItem[] allItems = Project.AllItems();
            return allItems.Where(item => item.ItemType == itemType).Cast<object>().ToArray();
        }

        public string GetFullName(object item)
        {
            IItem i = item as IItem;
            if (i != null)
            {
                return MockProject.GetFullName(i);
            }
            return null;
        }

        public void Dispose()
        {
            Project = null;
        }

    }

}