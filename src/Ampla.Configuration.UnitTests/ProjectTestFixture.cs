﻿using System;
using System.Collections.Generic;
using Ampla.Configuration.Framework;
using NUnit.Framework;

namespace Ampla.Configuration
{
    public abstract class ProjectTestFixture : TestFixture
    {
        protected override void OnSetUp()
        {
            base.OnSetUp();
            Project = new MockProject();
            List<IItemType> itemTypes = GetItemTypes();
            foreach (IItemType itemType in itemTypes)
            {
                Project.AddType(itemType);
            }
            Logger = new StringLogger(true);

            ProjectView = new MockProjectView(Project, itemTypes.Count > 0 ? itemTypes[0] : null);
        }

        protected MockProjectView ProjectView { get; private set; }

        protected override void OnTearDown()
        {
            Project = null;
            Logger = null;
            base.OnTearDown();
        }

        protected StringLogger Logger
        {
            get; private set;
        }

        protected abstract List<IItemType> GetItemTypes();

        protected MockProject Project { get; private set; }

        protected IProjectFactory Factory {
            get
            {
                return new MockProjectFactory(Project);
            }
        }

        protected IItem AssertItemExists(string fullName)
        {
            IItem item = Project.Find(fullName);
            Assert.That(item, Is.Not.Null, "Item '{0}' should exist.\r\nMessages: {1}", fullName, Logger);
            return item;
        }

        protected void AssertItemProperty<T>(MockItem item, string property, T expectedValue)
        {
            T actualValue = item.GetProperty<T>(property);
            Assert.That(actualValue, Is.EqualTo(expectedValue), "Property '{0}' not as expected.\r\n{1}", property, Logger);
        }

        protected void AssertItemExistsWithProperty<T>(string fullName, string property, T expectedValue)
        {
            IItem item = Project.Find(fullName);
            Assert.That(item, Is.Not.Null, "Item '{0}' should exist.\r\nMessages: {1}", fullName, Logger);
            T actualValue = item.GetProperty<T>(property);
            Assert.That(actualValue, Is.EqualTo(expectedValue), "Property '{0}' not as expected.\r\n{1}", property, Logger);
        }
        
        protected void AssertItemNotExists(string fullName)
        {
            Assert.That(Project.Exists(fullName), Is.False, "{0} should not exist", fullName);
        }

        protected void AssertInformationCount(int count)
        {
            Assert.That(Logger.InformationCount, Is.EqualTo(count), "Logger.InformationCount\r\n{0}", Logger);
        }

        protected void AssertWarningCount(int count)
        {
            Assert.That(Logger.WarningCount, Is.EqualTo(count), "Logger.WarningCount\r\n{0}", Logger);
        }

        protected void AssertItemCount(int count)
        {
            Assert.That(Project.TotalCount, Is.EqualTo(count), "Project.TotalCount\r\n{0}", Logger);
        }

        protected void AssertWarningsContain(string warning)
        {
            string messages = string.Join(Environment.NewLine, Logger.Warnings);
            Assert.That(messages, Is.StringContaining(warning), "{0}", Logger);
        }
    }
}