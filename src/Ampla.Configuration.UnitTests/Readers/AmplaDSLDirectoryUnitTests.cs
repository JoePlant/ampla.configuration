﻿using System;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration.Readers
{
    [TestFixture]
    public class AmplaDSLDirectoryUnitTests : TestFixture
    {
        private const string resources = @".\Resources\Add";
        private StringLogger logger;
        
        protected override void OnSetUp()
        {
            base.OnSetUp();
            logger = new StringLogger();
        }

        protected override void OnTearDown()
        {
            logger = null;
            base.OnTearDown();
        }
        
        [Test]
        public void Default()
        {
            AmplaDSLDirectory directory = new AmplaDSLDirectory(logger, resources, "*.*");
            Assert.That(directory.Entries, Is.Empty);
        }

        [Test]
        public void ReadWithInvalidDirectory()
        {
            AmplaDSLDirectory directory = new AmplaDSLDirectory(logger, resources + @"\Invalid", "*.*");
            directory.Read();

            Assert.That(directory.Entries, Is.Empty);
        }
        
        [Test]
        public void ReadWithNoMatchingFiles()
        {
            AmplaDSLDirectory directory = new AmplaDSLDirectory(logger, resources, "*.invalid");
            directory.Read();

            Assert.That(directory.Entries, Is.Empty);
        }

        [Test]
        public void ReadFolderMask()
        {
            AmplaDSLDirectory directory = new AmplaDSLDirectory(logger, resources, "Folder.*");
            directory.Read();

            Assert.That(directory.Entries, Is.Not.Empty);
            Assert.That(directory.Entries.Length, Is.EqualTo(1));
            AmplaDSL dsl = directory.Entries[0];

            Assert.That(dsl.Type, Is.EqualTo(typeof(Folder).FullName));
        }

        [Test]
        public void ReadAllFiles()
        {
            AmplaDSLDirectory directory = new AmplaDSLDirectory(logger, resources, "*.*");
            directory.Read();

            Assert.That(directory.Entries, Is.Not.Empty);
            Assert.That(directory.Entries.Length, Is.EqualTo(4));
        }

    }
}