﻿using System.Data;
using System.IO;
using System.Linq;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration.Readers
{
    [TestFixture]
    public class AmplaDSLReaderUnitTests : TestFixture
    {
        private StringLogger logger;

        protected override void OnSetUp()
        {
            base.OnSetUp();
            logger = new StringLogger(true);
        }

        [Test]
        public void ApplicationsFolder()
        {
            const string fileName = @".\Resources\Add\Folder.txt";
            Assert.That(File.Exists(fileName), Is.True);

            AmplaDSLReader reader = new AmplaDSLReader(logger, fileName);
            Assert.That(reader.Entry, Is.Null);
            reader.Read();
            Assert.That(reader.Entry, Is.Not.Null);

            AmplaDSL dsl = reader.Entry;
            Assert.That(dsl.Parent, Is.EqualTo(null));
            Assert.That(dsl.Type, Is.EqualTo(typeof(Folder).FullName));
            Assert.That(dsl.Values, Is.Not.Null);
            dsl.Values.AssertTable(new[] {"FullName"},
                        new []
                            {
                                new object[] {"Enterprise"},
                                new object[] {"Enterprise.Site"},
                                new object[] {"Enterprise.Site.Area"},
                            });

            Assert.That(logger.WarningCount, Is.EqualTo(0), "{0}", logger);

        }


        [Test]
        public void CauseCodes()
        {
            const string fileName = @".\Resources\Add\CauseCode.txt";
            Assert.That(File.Exists(fileName), Is.True);

            AmplaDSLReader reader = new AmplaDSLReader(logger,fileName);
            Assert.That(reader.Entry, Is.Null);
            reader.Read();
            Assert.That(reader.Entry, Is.Not.Null);

            AmplaDSL dsl = reader.Entry;
            Assert.That(dsl.Parent, Is.EqualTo("System Configuration.Lookup Lists.Cause Codes"));
            Assert.That(dsl.Type, Is.EqualTo(typeof(CauseCode).FullName));
            Assert.That(dsl.Values, Is.Not.Null);
            Assert.That(dsl.Values.Columns.Count, Is.EqualTo(2));
            Assert.That(dsl.Values.Columns[0].ColumnName, Is.EqualTo("Name"));
            Assert.That(dsl.Values.Columns[1].ColumnName, Is.EqualTo("CauseCodeId"));
            Assert.That(dsl.Values.Columns[1].DataType, Is.EqualTo(typeof(int)));
            Assert.That(dsl.Values.Rows.Count, Is.EqualTo(2));
            dsl.Values.AssertTable(new[] { "Name", "CauseCodeId" },
                        new []
                            {
                                new object[] {"Blocked", 10},
                                new object[] {"Torn Belt", 20},
                            });
            Assert.That(logger.WarningCount, Is.EqualTo(0), "{0}", logger);
        }

        [Test]
        public void EquipmentIds()
        {
            const string fileName = @".\Resources\Add\EquipmentId.txt";
            Assert.That(File.Exists(fileName), Is.True);

            AmplaDSLReader reader = new AmplaDSLReader(logger, fileName);
            Assert.That(reader.Entry, Is.Null);
            reader.Read();
            Assert.That(reader.Entry, Is.Not.Null);

            AmplaDSL dsl = reader.Entry;
            Assert.That(dsl.Parent, Is.EqualTo(null));
            Assert.That(dsl.Type, Is.EqualTo(typeof(Folder).FullName));
            Assert.That(dsl.Values, Is.Not.Null);
            dsl.Values.AssertTable(new[] { "FullName", "EquipmentId" },
                        new object[][]
                            {
                                new[] {"Enterprise", ""},
                                new[] {"Enterprise.Site", "S"},
                                new[] {"Enterprise.Site.Area 1", "S-A1"},
                                new[] {"Enterprise.Site.Area 2", "S-A2"},
                            });
            Assert.That(logger.WarningCount, Is.EqualTo(0), "{0}", logger);
        }

        [Test]
        public void ModifyUsers()
        {
            const string fileName = @".\Resources\Modify\Users.txt";
            Assert.That(File.Exists(fileName), Is.True);

            AmplaDSLReader reader = new AmplaDSLReader(logger, fileName);
            Assert.That(reader.Entry, Is.Null);
            reader.Read();
            Assert.That(reader.Entry, Is.Not.Null);

            AmplaDSL dsl = reader.Entry;
            Assert.That(dsl.Parent, Is.EqualTo("System Configuration.Users"));
            Assert.That(dsl.Type, Is.EqualTo(typeof(User).FullName ));
            Assert.That(dsl.Values, Is.Not.Null);
            dsl.Values.AssertTable(new[] { "Name", "Password" },
                        new[]
                            {
                                new object[] {"Joe", "secret"},
                            });

            Assert.That(logger.WarningCount, Is.EqualTo(0), "{0}", logger);
        }

        [Test]
        public void SelectReportingPoint()
        {
            const string fileName = @".\Resources\Add\ShiftField.txt";
            Assert.That(File.Exists(fileName), Is.True);

            AmplaDSLReader reader = new AmplaDSLReader(logger, fileName);
            Assert.That(reader.Entry, Is.Null);
            reader.Read();
            Assert.That(reader.Entry, Is.Not.Null);

            AmplaDSL dsl = reader.Entry;
            Assert.That(dsl.Parent, Is.EqualTo(null));
            Assert.That(dsl.FindType, Is.EqualTo(typeof(ReportingPoint).FullName));
            Assert.That(dsl.Type, Is.EqualTo(typeof(Field).FullName));
            Assert.That(dsl.Mode, Is.EqualTo("Add"));
            Assert.That(dsl.Values, Is.Not.Null);
            dsl.Values.AssertTable(new[] { "RelativeName", "Expression" },
                        new[]
                            {
                                new object[] {"Fields.Shift", "Current.Shift()"},
                            });

            Assert.That(logger.WarningCount, Is.EqualTo(0), "{0}", logger);
            
        }
    }
}