﻿using System.IO;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration.Readers
{
    [TestFixture]
    public class AmplaDSLWriterUnitTests : TemporaryFileTestFixture
    {
        private ILogger logger;

        public AmplaDSLWriterUnitTests() : base("txt")
        {
        }

        protected override void OnSetUp()
        {
            base.OnSetUp();
            logger = new StringLogger(true);
        }

        [Test]
        public void EmptyDSL()
        {
            AmplaDSL amplaDSL = new AmplaDSL();
            AmplaDSLWriter writer = new AmplaDSLWriter(logger, FileName, amplaDSL);
            writer.Write();

            Assert.That(File.Exists(FileName), Is.True);
            Assert.That(File.ReadAllLines(FileName), Is.EqualTo(new []
                {
                    "Type:=", 
                    "==="
                }));
        }

        [Test]
        public void TypeSpecified()
        {
            AmplaDSL amplaDSL = new AmplaDSL {Type = typeof (ReportingPoint).FullName};
            AmplaDSLWriter writer = new AmplaDSLWriter(logger, FileName, amplaDSL);
            writer.Write();

            Assert.That(File.Exists(FileName), Is.True);
            Assert.That(File.ReadAllLines(FileName), Is.EqualTo(new[]
                {
                    "Type:=Ampla.Configuration.Items.ReportingPoint",
                    "==="
                }));
        }

        [Test]
        public void ParentSpecified()
        {
            AmplaDSL amplaDSL = new AmplaDSL { Parent = "System Configuration.Users"};
            AmplaDSLWriter writer = new AmplaDSLWriter(logger, FileName, amplaDSL);
            writer.Write();

            Assert.That(File.Exists(FileName), Is.True);
            Assert.That(File.ReadAllLines(FileName), Is.EqualTo(new[]
                {
                    "Type:=",
                    "Parent:=System Configuration.Users",
                    "==="
                }));
        }

        [Test]
        public void ModeSpecified()
        {
            AmplaDSL amplaDSL = new AmplaDSL { Type = typeof(ReportingPoint).FullName, Mode = "Add"};
            AmplaDSLWriter writer = new AmplaDSLWriter(logger, FileName, amplaDSL);
            writer.Write();

            Assert.That(File.Exists(FileName), Is.True);
            Assert.That(File.ReadAllLines(FileName), Is.EqualTo(new[]
                {
                    "Type:=Ampla.Configuration.Items.ReportingPoint",
                    "Mode:=Add",
                    "==="
                }));
        }


        [Test]
        public void FullNameSpecified()
        {
            AmplaDSL amplaDSL = new AmplaDSL { Type = typeof(ReportingPoint).FullName };
            amplaDSL.AddColumn("FullName");
            amplaDSL.AddValues(new object[] { "Enterprise.Site.Area 1.Point" });
            amplaDSL.AddValues(new object[] { "Enterprise.Site.Area 2.Point" });
            AmplaDSLWriter writer = new AmplaDSLWriter(logger, FileName, amplaDSL);
            writer.Write();

            Assert.That(File.Exists(FileName), Is.True);
            Assert.That(File.ReadAllLines(FileName), Is.EqualTo(new[]
                {
                    "Type:=Ampla.Configuration.Items.ReportingPoint",
                    "===",
                    "FullName",
                    "Enterprise.Site.Area 1.Point",
                    "Enterprise.Site.Area 2.Point"
                }));
        }

        [Test]
        public void FullNameAndPropertySpecified()
        {
            AmplaDSL amplaDSL = new AmplaDSL { Type = typeof(Folder).FullName };
            amplaDSL.AddColumn("FullName");
            amplaDSL.AddColumn("EquipmentId");
            amplaDSL.AddValues(new object[] { "Enterprise","" });
            amplaDSL.AddValues(new object[] { "Enterprise.Site", "S" });
            amplaDSL.AddValues(new object[] { "Enterprise.Site.Area 1", "S.A1" });
            amplaDSL.AddValues(new object[] { "Enterprise.Site.Area 2", "S.A2" });
            AmplaDSLWriter writer = new AmplaDSLWriter(logger, FileName, amplaDSL);
            writer.Write();

            Assert.That(File.Exists(FileName), Is.True);
            Assert.That(File.ReadAllLines(FileName), Is.EqualTo(new[]
                {
                    "Type:=Ampla.Configuration.Items.Folder",
                    "===",
                    "FullName,EquipmentId",
                    "Enterprise,",
                    "Enterprise.Site,S",
                    "Enterprise.Site.Area 1,S.A1",
                    "Enterprise.Site.Area 2,S.A2"
                }));
        }
    }
}