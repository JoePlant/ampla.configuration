﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ampla.Configuration
{
    public class StringLogger : ILogger
    {
        private readonly StringBuilder builder = new StringBuilder();

        private readonly List<string> verbose = new List<string>();
        private readonly List<string> information = new List<string>();
        private readonly List<string> warning = new List<string>();

        private readonly bool outputToConsole;

        public StringLogger() : this(false)
        {
        }

        public StringLogger(bool outputToConsole)
        {
            this.outputToConsole = outputToConsole;
        }

        public void LogVerbose(string message)
        {
            builder.Append("[Verbose] ");
            builder.AppendLine(message);
            verbose.Add(message);
            if (outputToConsole)
            {
                Console.WriteLine(message);
            }
        }

        public void LogInformation(string message)
        {
            builder.Append("[Information] ");
            builder.AppendLine(message);
            information.Add(message);
            if (outputToConsole)
            {
                Console.WriteLine(message);
            }
        }

        public void LogWarning(string message)
        {
            builder.Append("[Warning] ");
            builder.AppendLine(message);
            warning.Add(message);
            if (outputToConsole)
            {
                Console.WriteLine(message);
            }
        }

        public override string ToString()
        {
            return builder.ToString();
        }

        public int VerboseCount
        {
            get { return verbose.Count; }
        }

        public int InformationCount
        {
            get { return information.Count; }
        }

        public int WarningCount
        {
            get { return warning.Count; }
        }

        public string[] Warnings
        {
            get { return warning.ToArray(); }
        }

    }
}