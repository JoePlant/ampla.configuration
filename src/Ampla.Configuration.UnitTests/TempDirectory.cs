﻿using System.IO;
using NUnit.Framework;

namespace Ampla.Configuration
{
    public class TempDirectory
    {
        private readonly string directoryName;
        private readonly string pattern;
        private int count;

        public static TempDirectory ForUnitTests(TestFixture testFixture, string fileExtension) 
        {
            string directory = @"Files\" + testFixture.GetType().Name;
            string pattern = "UnitTest_{0:00}." + fileExtension;
            TempDirectory tempDir = new TempDirectory(directory, pattern);
            EnsureDirectoryIsEmpty(directory);

            return tempDir;
        }

        public TempDirectory(string directoryName, string pattern)
        {
            this.directoryName = directoryName;
            this.pattern = pattern;
        }

        private static DirectoryInfo EnsureDirectoryIsEmpty(string directoryName)
        {
            DirectoryInfo directory = new DirectoryInfo(directoryName);
            if (directory.Exists)
            {
                foreach (FileInfo file in directory.GetFiles("*.*", SearchOption.AllDirectories))
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in directory.GetDirectories("*.*", SearchOption.AllDirectories))
                {
                    dir.Delete();
                }
            }
            else
            {
                directory.Create();
            }
            return new DirectoryInfo(directoryName);
        }

        public string GetSpecificDirectory(string relativeName)
        {
            string specificDirName = Path.Combine(directoryName, relativeName);
            
            DirectoryInfo directoryInfo = EnsureDirectoryIsEmpty(specificDirName);

            Assert.That(directoryInfo.Exists, Is.True, "Directory doesn't exist: {0}", directoryInfo.FullName);
            return directoryInfo.FullName;
        }

        public string GetSpecificFileName(string fileName)
        {
            FileInfo file = new FileInfo(Path.Combine(directoryName, fileName));
            if (file.Exists)
            {
                file.Delete();
            }
            Assert.That(file.Exists, Is.False, "File already exists: {0}", file.FullName);
            return file.FullName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetNextTemporaryFile()
        {
            string name = string.Format(pattern, ++count);
            return GetSpecificFileName(name);
        }
    }
}