﻿using System;
using NUnit.Framework;

namespace Ampla.Configuration
{
    [TestFixture]
    public abstract class TemporaryFileTestFixture : TestFixture
    {
        private readonly string extension;

        protected TemporaryFileTestFixture(string extension)
        {
            this.extension = extension;
        }

        protected TempDirectory TempDirectory { get; private set; }

        protected string FileName { get; private set; }

        protected override void OnFixtureSetUp()
        {
            base.OnFixtureSetUp();
            TempDirectory = TempDirectory.ForUnitTests(this, extension);
        }

        protected override void OnSetUp()
        {
            base.OnSetUp();
            FileName = TempDirectory.GetNextTemporaryFile();
        }
    }
}