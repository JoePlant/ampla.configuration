﻿using System.Collections.Generic;
using Ampla.Configuration.Framework;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration.Workers
{
    [TestFixture]
    public class AddChildItemsWorkerUnitTests : ProjectTestFixture
    {
        protected override List<IItemType> GetItemTypes()
        {
            return new List<IItemType>
                {
                    new RealItemType<Folder>(),
                    new RealItemType<User>(),
                    new RealItemType<CauseCode>(),
                    new RealItemType<Field>(),
                };
        }
        
        [Test]
        public void AddItemsOnly()
        {
            Project.AddItem("System Configuration");
            Project.AddItem("System Configuration.Users");

            AssertItemExists("System Configuration.Users");

            AmplaDSL amplaDsl = new AmplaDSL {Type = typeof(User).FullName, Parent = "System Configuration.Users", Mode = "Add"};
            amplaDsl.AddColumn("Name");
            amplaDsl.AddValues(new object[] {"Joe"});
            amplaDsl.AddValues(new object[] {"Frank"});
            AddChildItemsWorker worker = new AddChildItemsWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExists("System Configuration.Users.Joe");
            AssertItemExists("System Configuration.Users.Frank");

        }

        [Test]
        public void AddItemsWithEnumPropertyAsString()
        {
            Project.AddItem("System Configuration");
            Project.AddItem("System Configuration.Users");

            AssertItemExists("System Configuration.Users");

            AmplaDSL amplaDsl = new AmplaDSL { Type = typeof(User).FullName, Parent = "System Configuration.Users", Mode = "Add" };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("AuthenticationMode");
            amplaDsl.AddColumn("Password");
            amplaDsl.AddValues(new object[] { "Joe", "Any", "J123" });
            amplaDsl.AddValues(new object[] { "Frank", "WindowsIntegrated", null });
            AddChildItemsWorker worker = new AddChildItemsWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExistsWithProperty("System Configuration.Users.Joe", "AuthenticationMode", UserAuthenticationMode.Any);
            AssertItemExistsWithProperty("System Configuration.Users.Frank", "AuthenticationMode", UserAuthenticationMode.WindowsIntegrated);
            AssertItemExistsWithProperty("System Configuration.Users.Joe", "Password", "J123");
            AssertItemExistsWithProperty<string>("System Configuration.Users.Frank", "Password", null);
        }

        [Test]
        public void AddItemsWithProperties()
        {
            Project.AddItem("System Configuration");
            Project.AddItem("System Configuration.Lookup Lists");
            Project.AddItem("System Configuration.Lookup Lists.Causes");

            AssertItemExists("System Configuration.Lookup Lists.Causes");

            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Type = typeof (CauseCode).FullName,
                    Parent = "System Configuration.Lookup Lists.Causes",
                    Mode = "Add"
                };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("CauseCodeId", typeof(int));
            amplaDsl.AddValues(new object[] { "Broken", 100});
            amplaDsl.AddValues(new object[] { "Stalled", 200 });
            AddChildItemsWorker worker = new AddChildItemsWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExistsWithProperty("System Configuration.Lookup Lists.Causes.Broken", "CauseCodeId", 100);
            AssertItemExistsWithProperty("System Configuration.Lookup Lists.Causes.Stalled", "CauseCodeId", 200);
        }

        [Test]
        public void WithExistingItems()
        {
            Project.AddItem("System Configuration");
            Project.AddItem("System Configuration.Lookup Lists");
            Project.AddItem("System Configuration.Lookup Lists.Causes");
            Project.AddItem("System Configuration.Lookup Lists.Causes");

            AssertItemExists("System Configuration.Lookup Lists.Causes");

            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(CauseCode).FullName,
                Parent = "System Configuration.Lookup Lists.Causes",
                Mode = "Add"
            };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("CauseCodeId", typeof(int));
            amplaDsl.AddValues(new object[] { "Broken", 100 });
            amplaDsl.AddValues(new object[] { "Stalled", 200 });
            AddChildItemsWorker worker = new AddChildItemsWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExistsWithProperty("System Configuration.Lookup Lists.Causes.Broken", "CauseCodeId", 100);
            AssertItemExistsWithProperty("System Configuration.Lookup Lists.Causes.Stalled", "CauseCodeId", 200);
        }

        [Test]
        public void AddItemsWithPropertiesOfString()
        {
            Project.AddItem("System Configuration");
            Project.AddItem("System Configuration.Lookup Lists");
            Project.AddItem("System Configuration.Lookup Lists.Causes");

            AssertItemExists("System Configuration.Lookup Lists.Causes");

            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(CauseCode).FullName,
                Parent = "System Configuration.Lookup Lists.Causes",
                Mode = "Add"
            };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("CauseCodeId");
            amplaDsl.AddValues(new object[] { "Broken", "100" });
            amplaDsl.AddValues(new object[] { "Stalled", "200" });
            AddChildItemsWorker worker = new AddChildItemsWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExistsWithProperty("System Configuration.Lookup Lists.Causes.Broken", "CauseCodeId", 100);
            AssertItemExistsWithProperty("System Configuration.Lookup Lists.Causes.Stalled", "CauseCodeId", 200);
        }

        [Test]
        public void AddItemWithComplexProperty()
        {
            Project.AddItem("Folder");
            Project.AddItem("Folder.Point");
            Project.AddItem("Folder.Point.Fields");

            AssertItemExists("Folder.Point.Fields");

            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Field).FullName,
                Parent = "Folder.Point.Fields",
                Mode = "Add"
            };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("Expression");
            amplaDsl.AddValues(new object[] { "Field 10", "10" });
            amplaDsl.AddValues(new object[] { "Field 20", "10 * 2" });
            AddChildItemsWorker worker = new AddChildItemsWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExistsWithProperty("Folder.Point.Fields.Field 10", "Expression", new Expression {Text = "10"});
            AssertItemExistsWithProperty("Folder.Point.Fields.Field 20", "Expression", new Expression { Text = "10 * 2" });
        }


        [Test]
        public void AddItemWithComplexPropertyWrongType()
        {
            Project.AddItem("Folder");
            Project.AddItem("Folder.Point");
            Project.AddItem("Folder.Point.Fields");

            AssertItemExists("Folder.Point.Fields");

            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Field).FullName,
                Parent = "Folder.Point.Fields",
                Mode = "Add"
            };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("Expression", typeof(int));
            amplaDsl.AddValues(new object[] { "Field 10", 10 });
            AddChildItemsWorker worker = new AddChildItemsWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExistsWithProperty<Expression>("Folder.Point.Fields.Field 10", "Expression", null);
        }


        [Test]
        public void AddItemsWithMissingParent()
        {
            Project.AddItem("System Configuration");
            Project.AddItem("System Configuration.Lookup Lists");
            Project.AddItem("System Configuration.Lookup Lists.Causes");

            AssertItemExists("System Configuration.Lookup Lists.Causes");

            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Type = typeof (CauseCode).FullName,
                    Parent = "System Configuration.Lookup Lists.Invalid",
                    Mode = "Add"
                };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddValues(new object[] { "Should not add" });
            AddChildItemsWorker worker = new AddChildItemsWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertWarningsContain("System Configuration.Lookup Lists.Invalid");

            AssertItemNotExists("System Configuration.Lookup Lists.Causes.Should not add");
        }

        [Test]
        public void AddItemsWithMissingMode()
        {
            Project.AddItem("System Configuration");
            Project.AddItem("System Configuration.Lookup Lists");
            Project.AddItem("System Configuration.Lookup Lists.Causes");

            AssertItemExists("System Configuration.Lookup Lists.Causes");

            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(CauseCode).FullName,
                Parent = "System Configuration.Lookup Lists.Causes",
                //Mode = "Add"
            };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddValues(new object[] { "Should not add" });
            AddChildItemsWorker worker = new AddChildItemsWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertWarningsContain("Mode:=Add");

            AssertItemNotExists("System Configuration.Lookup Lists.Causes.Should not add");
        }
    }
}