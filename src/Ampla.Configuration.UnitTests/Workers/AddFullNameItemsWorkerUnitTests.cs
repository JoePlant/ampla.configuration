﻿using System.Collections.Generic;
using Ampla.Configuration.Framework;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration.Workers
{
    [TestFixture]
    public class AddFullNameItemsWorkerUnitTests : ProjectTestFixture
    {
        protected override List<IItemType> GetItemTypes()
        {
            return new List<IItemType>
                {
                    new RealItemType<Folder>(),
                    new RealItemType<Field>()
                };
        }
        
        [Test]
        public void AddItems()
        {
            AmplaDSL amplaDsl = new AmplaDSL {Type = typeof(Folder).FullName, Mode = "Add"};
            amplaDsl.AddColumn("FullName");
            amplaDsl.AddValues(new object[] {"Enterprise"});
            amplaDsl.AddValues(new object[] {"Enterprise.Site"});
            amplaDsl.AddValues(new object[] { "Enterprise.Site.Area" });
            AddFullNameItemsWorker worker = new AddFullNameItemsWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExists("Enterprise");
            AssertItemExists("Enterprise.Site");
            AssertItemExists("Enterprise.Site.Area");
        }

        [Test]
        public void AddItemsWithProperties()
        {
            AmplaDSL amplaDsl = new AmplaDSL { Type = typeof(Folder).FullName, Mode = "Add" };
            amplaDsl.AddColumn("FullName");
            amplaDsl.AddColumn("EquipmentId");
            amplaDsl.AddValues(new object[] { "Enterprise", "" });
            amplaDsl.AddValues(new object[] { "Enterprise.Site", "S" });
            amplaDsl.AddValues(new object[] { "Enterprise.Site.Area", "A" });
            AddFullNameItemsWorker worker = new AddFullNameItemsWorker(Factory, Logger, amplaDsl); 
            worker.Execute();

            AssertItemExistsWithProperty("Enterprise", "EquipmentId", "");
            AssertItemExistsWithProperty("Enterprise.Site", "EquipmentId", "S");
            AssertItemExistsWithProperty("Enterprise.Site.Area", "EquipmentId", "A");
        }
        
        [Test]
        public void InvalidItemsWithProperties()
        {
            AssertItemNotExists("Enterprise.Site");

            AmplaDSL amplaDsl = new AmplaDSL { Type = typeof(Field).FullName, Mode = "Add" };
            amplaDsl.AddColumn("FullName");
            amplaDsl.AddColumn("Expression");
            amplaDsl.AddValues(new object[] { "Enterprise.Site.Point.Fields.Field A", "A" });
            AddFullNameItemsWorker worker = new AddFullNameItemsWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertWarningsContain("Enterprise.Site.Point.Fields ");
        }

        [Test]
        public void MissingMode()
        {
            AssertItemNotExists("Enterprise.Site");

            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Type = typeof (Field).FullName,
                    //Mode = "Add"
                };
            amplaDsl.AddColumn("FullName");
            amplaDsl.AddColumn("Expression");
            amplaDsl.AddValues(new object[] { "Enterprise.Site.Point.Fields.Field A", "A" });
            AddFullNameItemsWorker worker = new AddFullNameItemsWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertWarningsContain("Mode:=Add");
        }

    }
}