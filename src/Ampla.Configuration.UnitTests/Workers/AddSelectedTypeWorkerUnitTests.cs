﻿using System.Collections.Generic;
using Ampla.Configuration.Framework;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration.Workers
{
    [TestFixture]
    public class AddSelectedTypeWorkerUnitTests : ProjectTestFixture
    {
        private RealItemType<ReportingPoint> reportingPointType;
        private RealItemType<Folder> folderType;
        private RealItemType<Field> fieldType;

        protected override List<IItemType> GetItemTypes()
        {
            reportingPointType = new RealItemType<ReportingPoint>();
            folderType = new RealItemType<Folder>();
            fieldType = new RealItemType<Field>();
            return new List<IItemType>
                {
                    folderType,
                    new RealItemType<User>(),
                    new RealItemType<CauseCode>(),
                    reportingPointType,
                    fieldType,
                };
        }

        [Test]
        public void AddDirectChild()
        {
            Project.AddItem("Enterprise")
                .Add(folderType, "Site")
                .Add(reportingPointType, "Point");

            AssertItemExists("Enterprise.Site.Point");

            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Type = typeof (Field).FullName,
                    FindType = typeof(ReportingPoint).FullName,
                    Mode = "Add"
                };
            amplaDsl.AddColumn("RelativeName");
            amplaDsl.AddValues(new object[] {"Fields"});
            AddSelectedTypeWorker worker = new AddSelectedTypeWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExists("Enterprise.Site.Point.Fields");
        }

        [Test]
        public void AddRelativeChildren()
        {
            Project.AddItem("Enterprise")
                .Add(folderType, "Site")
                .Add(reportingPointType, "Point")
                .Add(folderType, "Fields");

            AssertItemExists("Enterprise.Site.Point.Fields");

            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Field).FullName,
                FindType = typeof(ReportingPoint).FullName,
                Mode = "Add"
            };
            amplaDsl.AddColumn("RelativeName");
            amplaDsl.AddValues(new object[] { "Fields.Field 1" });
            amplaDsl.AddValues(new object[] { "Fields.Field 2" });
            AddSelectedTypeWorker worker = new AddSelectedTypeWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExists("Enterprise.Site.Point.Fields.Field 1");
            AssertItemExists("Enterprise.Site.Point.Fields.Field 2");
        }

        [Test]
        public void AddDirectChildWithExisting()
        {
            Project.AddItem("Enterprise")
                .Add(folderType, "Site")
                .Add(reportingPointType, "Point")
                .Add(folderType, "Fields");

            AssertItemExists("Enterprise.Site.Point.Fields");

            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Field).FullName,
                FindType = typeof(ReportingPoint).FullName,
                Mode = "Add"
            };
            amplaDsl.AddColumn("RelativeName");
            amplaDsl.AddValues(new object[] { "Fields" });
            AddSelectedTypeWorker worker = new AddSelectedTypeWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExists("Enterprise.Site.Point.Fields");
            AssertItemNotExists("Enterprise.Site.Point.Fields.Fields");
        }

        [Test]
        public void AddRelativeChildrenWithExisting()
        {
            Project.AddItem("Enterprise")
                .Add(folderType, "Site")
                .Add(reportingPointType, "Point")
                .Add(folderType, "Fields").Add(fieldType, "Field 1");

            AssertItemExists("Enterprise.Site.Point.Fields.Field 1");
            AssertItemNotExists("Enterprise.Site.Point.Fields.Field 2");

            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Field).FullName,
                FindType = typeof(ReportingPoint).FullName,
                Mode = "Add"
            };
            amplaDsl.AddColumn("RelativeName");
            amplaDsl.AddValues(new object[] { "Fields.Field 1" });
            amplaDsl.AddValues(new object[] { "Fields.Field 2" });
            AddSelectedTypeWorker worker = new AddSelectedTypeWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExists("Enterprise.Site.Point.Fields.Field 1");
            AssertItemExists("Enterprise.Site.Point.Fields.Field 2");
        }
        
        [Test]
        public void AddItemsWithMissingParent()
        {
            Project.AddItem("Enterprise")
                .Add(folderType, "Site")
                .Add(reportingPointType, "Point");

            AssertItemNotExists("Enterprise.Site.Point.Fields");

            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Field).FullName,
                FindType = typeof(ReportingPoint).FullName,
                Mode = "Add"
            };
            amplaDsl.AddColumn("RelativeName");
            amplaDsl.AddValues(new object[] { "Fields.Field 1" });
            amplaDsl.AddValues(new object[] { "Fields.Field 2" });
            AddSelectedTypeWorker worker = new AddSelectedTypeWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertWarningsContain("Enterprise.Site.Point.Fields");

            AssertItemNotExists("Enterprise.Site.Point.Fields.Field 1");
            AssertItemNotExists("Enterprise.Site.Point.Fields.Field 2");
        }

        [Test]
        public void AddItemsWithMissingMode()
        {
            Project.AddItem("Enterprise")
                   .Add(folderType, "Site")
                   .Add(reportingPointType, "Point");

            AssertItemExists("Enterprise.Site.Point");
            AssertItemNotExists("Enterprise.Site.Point.Fields");

            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Type = typeof (Field).FullName,
                    FindType = typeof (ReportingPoint).FullName,
                    //Mode = "Add"
                };
            amplaDsl.AddColumn("RelativeName");
            amplaDsl.AddValues(new object[] {"Fields"});
            AddSelectedTypeWorker worker = new AddSelectedTypeWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemNotExists("Enterprise.Site.Point.Fields");

            AssertWarningsContain("Mode:=Add");
        }
    }
}