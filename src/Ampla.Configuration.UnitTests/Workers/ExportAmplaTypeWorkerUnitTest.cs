﻿using System.Collections.Generic;
using Ampla.Configuration.Framework;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration.Workers
{
    [TestFixture]
    public class ExportAmplaTypeWorkerUnitTest : ProjectTestFixture
    {
        private readonly RealItemType<Folder> folderItemType = new RealItemType<Folder>();
        private readonly RealItemType<ReportingPoint> reportingPointType = new RealItemType<ReportingPoint>();
        private readonly RealItemType<Field> fieldType = new RealItemType<Field>();
        private readonly RealItemType<User> userType = new RealItemType<User>();
        
        protected override List<IItemType> GetItemTypes()
        {
            return new List<IItemType>{ folderItemType, reportingPointType, fieldType, userType};
        }

        [Test]
        public void ExportEmpty()
        {
            Assert.That(Project.TotalCount, Is.EqualTo(0));

            Assert.That(Project.GetItemType(typeof(Folder).FullName), Is.Not.Null);

            ExportAmplaTypeWorker worker = new ExportAmplaTypeWorker(Factory, Logger, typeof(Folder).FullName);
            worker.Execute();
            Assert.That(worker.AmplaDSL.Type, Is.EqualTo(folderItemType.ItemType.FullName));
            Assert.That(worker.AmplaDSL.Parent, Is.Null);
            Assert.That(worker.AmplaDSL.FindType, Is.Null);
            Assert.That(worker.AmplaDSL.Mode, Is.EqualTo("Export"));
            worker.AmplaDSL.Values.AssertTable(new[] { "FullName", "EquipmentId" }, new object[0][]);
        }

        [Test]
        public void Export1Item()
        {
            Project.AddItem("Enterprise");
            Project.SetPropertyValue("Enterprise", "EquipmentId", "ENT");

            Assert.That(Project.GetItemType(typeof(Folder).FullName), Is.Not.Null);

            Assert.That(Project.TotalCount, Is.EqualTo(1));
            ExportAmplaTypeWorker worker = new ExportAmplaTypeWorker(Factory, Logger, typeof(Folder).FullName);
            worker.Execute();
            Assert.That(worker.AmplaDSL.Type, Is.EqualTo(folderItemType.ItemType.FullName));
            Assert.That(worker.AmplaDSL.Parent, Is.Null);
            Assert.That(worker.AmplaDSL.FindType, Is.Null);
            Assert.That(worker.AmplaDSL.Mode, Is.EqualTo("Export"));
            worker.AmplaDSL.Values.AssertTable(new[] { "FullName", "EquipmentId" }, new[]
                {
                    new object[] {"Enterprise", "ENT"}
                });
        }

        [Test]
        public void ExportInvalidType()
        {
            Assert.That(Project.TotalCount, Is.EqualTo(0));

            Assert.That(Project.GetItemType(typeof(Folder).FullName), Is.Not.Null);

            ExportAmplaTypeWorker worker = new ExportAmplaTypeWorker(Factory, Logger, typeof(int).FullName);
            worker.Execute();
            Assert.That(worker.AmplaDSL, Is.Null);
        }

        [Test]
        public void ExportReportingPoint()
        {
            IItem item = Project.AddItem("Enterprise");
            Project.SetPropertyValue("Enterprise", "EquipmentId", "ENT");
            item.Add(folderItemType, "Area").Add(reportingPointType, "Point");

            Assert.That(Project.TotalCount, Is.EqualTo(3));

            Assert.That(Project.GetItemType(typeof(Folder).FullName), Is.Not.Null);

            ExportAmplaTypeWorker worker = new ExportAmplaTypeWorker(Factory, Logger, typeof(ReportingPoint).FullName);
            worker.Execute();
            Assert.That(worker.AmplaDSL.Type, Is.EqualTo(reportingPointType.ItemType.FullName));
            Assert.That(worker.AmplaDSL.Parent, Is.Null);
            Assert.That(worker.AmplaDSL.FindType, Is.Null);
            Assert.That(worker.AmplaDSL.Mode, Is.EqualTo("Export"));
            worker.AmplaDSL.Values.AssertTable(new[] { "FullName" }, new[]
                {
                    new object[] {"Enterprise.Area.Point"}
                });
        }

        [Test]
        public void ExportFields()
        {
            IItem reportingPoint = Project.AddItem("Enterprise")
                   .Add(folderItemType, "Area")
                   .Add(reportingPointType, "Point");
            
            reportingPoint.Add(fieldType, "Field 1");
            reportingPoint.Add(fieldType, "Field 2");

            Project.SetPropertyValue("Enterprise", "EquipmentId", "ENT");
            Project.SetPropertyValue("Enterprise.Area.Point.Field 1", "Description", "Field 1 Description");
            Project.SetPropertyValue("Enterprise.Area.Point.Field 2", "Description", "Field 2 Description");

            Assert.That(Project.TotalCount, Is.EqualTo(5));

            Assert.That(Project.GetItemType(typeof(Field).FullName), Is.Not.Null);

            ExportAmplaTypeWorker worker = new ExportAmplaTypeWorker(Factory, Logger, typeof(Field).FullName);
            worker.Execute();
            Assert.That(worker.AmplaDSL.Type, Is.EqualTo(fieldType.ItemType.FullName));
            Assert.That(worker.AmplaDSL.Parent, Is.Null);
            Assert.That(worker.AmplaDSL.FindType, Is.Null);
            Assert.That(worker.AmplaDSL.Mode, Is.EqualTo("Export"));
            worker.AmplaDSL.Values.AssertTable(new[] { "FullName", "Description" }, new[]
                {
                    new object[] {"Enterprise.Area.Point.Field 1", "Field 1 Description"},
                    new object[] {"Enterprise.Area.Point.Field 2", "Field 2 Description"}
                });
        }

        [Test]
        public void ExportUsers()
        {
            IItem users = Project.AddItem("System Configuration")
                                 .Add(folderItemType, "Users");

            users.Add(userType, "Harry");
            users.Add(userType, "Sally");

            Project.SetPropertyValue("System Configuration.Users.Harry", "Password", "royal");
            Project.SetPropertyValue("System Configuration.Users.Sally", "Password", "secret");

            Project.SetPropertyValue("System Configuration.Users.Harry", "AuthenticationMode", UserAuthenticationMode.Simple);
            Project.SetPropertyValue("System Configuration.Users.Sally", "AuthenticationMode", UserAuthenticationMode.Any);

            Assert.That(Project.TotalCount, Is.EqualTo(4));

            Assert.That(Project.GetItemType(typeof(User).FullName), Is.Not.Null);

            ExportAmplaTypeWorker worker = new ExportAmplaTypeWorker(Factory, Logger, typeof(User).FullName);
            worker.Execute();
            Assert.That(worker.AmplaDSL.Type, Is.EqualTo(userType.ItemType.FullName));
            Assert.That(worker.AmplaDSL.Parent, Is.Null);
            Assert.That(worker.AmplaDSL.FindType, Is.Null);
            Assert.That(worker.AmplaDSL.Mode, Is.EqualTo("Export"));
            worker.AmplaDSL.Values.AssertTable(new[] { "FullName", "AuthenticationMode", "Password" }, new[]
                {
                    new object[] {"System Configuration.Users.Harry", "Simple", "royal"},
                    new object[] {"System Configuration.Users.Sally", "Any", "secret"}
                });
        }
    }
}