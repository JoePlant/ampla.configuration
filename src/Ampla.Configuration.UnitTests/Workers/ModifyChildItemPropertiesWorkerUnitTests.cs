﻿using System.Collections.Generic;
using Ampla.Configuration.Framework;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration.Workers
{
    [TestFixture]
    public class ModifyChildItemPropertiesWorkerUnitTests : ProjectTestFixture
    {
        private readonly IItemType userType = new RealItemType<User>();
        private readonly IItemType folderType = new RealItemType<Folder>();
        private readonly IItemType causeCodeType = new RealItemType<CauseCode>();

        protected override List<IItemType> GetItemTypes()
        {
            return new List<IItemType>
                {
                    folderType,
                    causeCodeType,
                    userType
                };
        }

        [Test]
        public void ModifyUsersPassword()
        {
            Project.AddItem("System Configuration");
            IItem item = Project.AddItem("System Configuration.Users");
            item.Add(userType, "Joe");

            AssertItemExistsWithProperty("System Configuration.Users.Joe", "Password", (string)null);

            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Type = typeof (User).FullName,
                    Mode = "Modify",
                    Parent = "System Configuration.Users"
                };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("Password");
            amplaDsl.AddValues(new object[] { "Joe", "secret" });
            AmplaDSLWorker worker = new ModifyChildItemPropertiesWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExistsWithProperty("System Configuration.Users.Joe", "Password", "secret");
        }

        [Test]
        public void IncorrectMode()
        {
            Project.AddItem("System Configuration");
            IItem item = Project.AddItem("System Configuration.Users");
            item.Add(userType, "Joe");

            AssertItemExistsWithProperty("System Configuration.Users.Joe", "Password", (string)null);

            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Type = typeof (User).FullName,
                    Mode = "Add",  // Mode = "Modify",
                    Parent = "System Configuration.Users"
                };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("Password");
            amplaDsl.AddValues(new object[] { "Joe", "secret" });
            AmplaDSLWorker worker = new ModifyChildItemPropertiesWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertWarningsContain("Mode:=Add");

            AssertItemExistsWithProperty("System Configuration.Users.Joe", "Password", (string)null);
        }

        [Test]
        public void ModifyEnumPropertyAsString()
        {
            Project.AddItem("System Configuration");
            IItem item = Project.AddItem("System Configuration.Users");
            item.Add(userType, "Joe");

            AssertItemExistsWithProperty("System Configuration.Users.Joe", "AuthenticationMode", UserAuthenticationMode.WindowsIntegrated);

            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Type = typeof (User).FullName,
                    Mode = "Modify",
                    Parent = "System Configuration.Users"
                };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("AuthenticationMode");
            amplaDsl.AddValues(new object[] { "Joe", "Any"});
            AmplaDSLWorker worker = new ModifyChildItemPropertiesWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExistsWithProperty("System Configuration.Users.Joe", "AuthenticationMode", UserAuthenticationMode.Any);
        }

        [Test]
        public void WrongTypeOfItem()
        {
            Project.AddItem("System Configuration");
            IItem item = Project.AddItem("System Configuration.Users");
            item.Add(userType, "Joe");

            AssertItemExists("System Configuration.Users.Joe");

            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Type = typeof (CauseCode).FullName,
                    Mode = "Modify",
                    Parent = "System Configuration.Users",
                };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("Password");
            amplaDsl.AddValues(new object[] { "Joe", "secret" });
            AmplaDSLWorker worker = new ModifyChildItemPropertiesWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertItemExistsWithProperty("System Configuration.Users.Joe", "Password", (string)null);

            AssertWarningsContain("Ampla.Configuration.Items.CauseCode");
        }

        [Test]
        public void ModifyPropertyWithMissingItem()
        {
            Project.AddItem("Enterprise");
            Project.AddItem("Enterprise.Site");
            Project.AddItem("Enterprise.Site.Area");

            AssertItemExists("Enterprise.Site.Area");
            AssertItemNotExists("Enterprise.Site.Area 2");

            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Type = typeof (Folder).FullName,
                    Parent = "Enterprise.Site",
                    Mode = "Modify"
                };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("EquipmentId");
            amplaDsl.AddValues(new object[] { "Area 2", "E.S.A" });
            AmplaDSLWorker worker = new ModifyChildItemPropertiesWorker(Factory, Logger, amplaDsl);
            worker.Execute();

            AssertWarningsContain("Enterprise.Site.Area 2");

            AssertItemExists("Enterprise.Site.Area");
            AssertItemNotExists("Enterprise.Site.Area 2");
        }
    }
}