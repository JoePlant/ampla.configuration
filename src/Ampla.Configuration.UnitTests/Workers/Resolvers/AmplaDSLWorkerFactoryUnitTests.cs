﻿using System.Collections.Generic;
using Ampla.Configuration.Framework;
using Ampla.Configuration.Items;
using NUnit.Framework;

namespace Ampla.Configuration.Workers.Resolvers
{
    [TestFixture]
    public class AmplaDSLWorkerFactoryUnitTests : ProjectTestFixture
    {
        protected override List<IItemType> GetItemTypes()
        {
            return new List<IItemType> {
                new RealItemType<Folder>(),
                new RealItemType<ReportingPoint>(),
                new RealItemType<Field>(),
                new RealItemType<User>(),
            };
        }

        [Test]
        public void NoTypeSpecified()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Parent = "Enterprise.Site.Area",
                    Mode = "Add"
                };
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);

            string messages = Logger.ToString();
            Assert.That(worker, Is.Null, messages);
            Assert.That(messages, Is.Not.Empty, messages);
            Assert.That(messages, Is.StringContaining("Type"));
        }

        [Test]
        public void WithParentAndName()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Type = typeof(User).FullName,
                    Parent = "System Configuration.Users",
                    Mode = "Add"
                };
            amplaDsl.AddColumn("Name");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            Assert.That(worker, Is.TypeOf<AddChildItemsWorker>(), Logger.ToString());
            Assert.That(Logger.ToString(), Is.Empty);
        }

        [Test]
        public void ModifyWithParentAndFullName()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(User).FullName,
                Parent = "System Configuration.Users",
                Mode = "Modify"
            };
            amplaDsl.AddColumn("FullName");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.Null, messages);
            Assert.That(messages, Is.Not.Empty, messages);
            Assert.That(messages, Is.StringContaining("Name"));
        }

        [Test]
        public void ModifyWithParentAndNoName()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(User).FullName,
                Parent = "System Configuration.Users",
                Mode = "Modify"
            };
            amplaDsl.AddColumn("Password");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.Null, messages);
            Assert.That(messages, Is.Not.Empty, messages);
            Assert.That(messages, Is.StringContaining("Name"));
        }

        [Test]
        public void WithFullNameColumn()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Folder).FullName,
                Mode = "Add"
            };
            amplaDsl.AddColumn("FullName");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.TypeOf<AddFullNameItemsWorker>(), messages);
            Assert.That(messages, Is.Empty);
        }

        [Test]
        public void InvalidType()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = "Citect.Ampla.Broken.Type",
                Mode = "Add"
            };
            amplaDsl.AddColumn("FullName");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.Null, messages);
            Assert.That(messages, Is.Not.Empty, messages);
            Assert.That(messages, Is.StringContaining("'Citect.Ampla.Broken.Type'"));
        }

        [Test]
        public void WithFullNameAndNameColumns()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Folder).FullName,
                Mode = "Add"
            };
            amplaDsl.AddColumn("FullName");
            amplaDsl.AddColumn("Name");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.Null, messages);
            Assert.That(messages, Is.Not.Empty, messages);
            Assert.That(messages, Is.StringContaining("FullName"));
        }

        [Test]
        public void WithParentAndFullNameAndNameColumns()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Folder).FullName,
                Parent = "Enterprise.Site",
                Mode = "Add"
            };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("FullName");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.Null, messages);
            Assert.That(messages, Is.Not.Empty, messages);
            Assert.That(messages, Is.StringContaining("FullName"));
        }

        [Test]
        public void ModifyWithParentAndFullNameAndNameColumns()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Folder).FullName,
                Parent = "Enterprise.Site",
                Mode = "Modify"
            };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("FullName");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.Null, messages);
            Assert.That(messages, Is.Not.Empty, messages);
            Assert.That(messages, Is.StringContaining("FullName"));
        }

        [Test]
        public void WithParentAndFullNameColumns()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Folder).FullName,
                Parent = "Enterprise.Site",
                Mode = "Add"
            };
            amplaDsl.AddColumn("FullName");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.Null, messages);
            Assert.That(messages, Is.Not.Empty, messages);
            Assert.That(messages, Is.StringContaining("Name"));
        }

        [Test]
        public void ModifyProperties()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Folder).FullName,
                Mode = "Modify",
            };
            amplaDsl.AddColumn("FullName");
            amplaDsl.AddColumn("EquipmentId");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.TypeOf<ModifyFullNamePropertiesWorker>(), messages);
            Assert.That(worker.Name, Is.StringContaining("Modify"));
            Assert.That(messages, Is.Empty, messages);
        }

        [Test]
        public void ModifyPropertiesNameOnly()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Folder).FullName,
                Mode = "Modify",
            };
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("EquipmentId");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.Null);
            Assert.That(messages, Is.Not.Empty, messages);
            Assert.That(messages, Is.StringContaining("FullName"));
        }

        [Test]
        public void ModifyChildItemProperties()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
                {
                    Type = typeof(User).FullName,
                    Parent = "System Configuration.Users",
                    Mode = "Modify"
                };            
            amplaDsl.AddColumn("Name");
            amplaDsl.AddColumn("Password");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.TypeOf<ModifyChildItemPropertiesWorker>(), messages);
            Assert.That(worker.Name, Is.StringContaining("Modify"));
            Assert.That(messages, Is.Empty, messages);
        }

        [Test]
        public void InvalidMode()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Folder).FullName,
                Mode = "Add-Modify"
            };
            amplaDsl.AddColumn("FullName");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.Null, messages);
            Assert.That(messages, Is.Not.Empty, messages);
            Assert.That(messages, Is.StringContaining("'Add-Modify'"));
        }

        [Test]
        public void SelectReportingPoint()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Field).FullName,
                FindType = typeof(ReportingPoint).FullName,
                Mode = "Add"
            };
            amplaDsl.AddColumn("RelativeName");
            amplaDsl.AddColumn("Expression");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.TypeOf<AddSelectedTypeWorker>(), messages);
            Assert.That(worker.Name, Is.StringContaining("AddSelectedType"));
            Assert.That(messages, Is.Empty, messages);
        }

        [Test]
        public void BothFindTypeAndParentSpecified()
        {
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();
            AmplaDSL amplaDsl = new AmplaDSL
            {
                Type = typeof(Field).FullName,
                Parent = "Enterprise.Site.Area.Point",
                FindType = typeof(ReportingPoint).FullName,
                Mode = "Add"
            };
            amplaDsl.AddColumn("FullName");
            IProjectWorker worker = factory.Create(Factory, Logger, amplaDsl);
            string messages = Logger.ToString();
            Assert.That(worker, Is.Null, messages);
            Assert.That(messages, Is.Not.Empty, messages);
            Assert.That(messages, Is.StringContaining("Parent").And.StringContaining("FindType"));
        }
    }
}