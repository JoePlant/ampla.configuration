﻿using System;
using System.Collections.Generic;
using System.Data;


namespace Ampla.Configuration
{
    public class AmplaDSL
    {
        private static readonly List<string> ValidModes = new List<string> {"Add", "Modify"};

        public AmplaDSL()
        {
            dataTable = new DataTable("Values");
        }

        public string Parent { get; set; }

        public string Type { get; set; }

        public string Mode { get; set; }

        public string FindType { get; set; }

        public string Source { get; set; }

        public IList<string> GetColumns()
        {
            List<string> columns = new List<string>();
            foreach (DataColumn dataColumn in dataTable.Columns)
            {
                columns.Add(dataColumn.ColumnName);
            }
            return columns.AsReadOnly();
        }

        public DataTable Values
        {
            get { return dataTable; }
        }

        private readonly DataTable dataTable; 
        
        public bool SetHeader(string header, string value)
        {
            bool result = false;
            switch (header)
            {
                case "FindType":
                    {
                        FindType = value;
                        result = true;
                        break;
                    }
                case "Parent":
                    {
                        Parent = value;
                        result = true;
                        break;
                    }
                case "Type":
                    {
                        Type = value;
                        result = true;
                        break;
                    }
                case "Mode":
                    {
                        if (ValidModes.Contains(value))
                        {
                            Mode = value;
                            result = true;
                        }
                        break;
                    }
            }
            return result;
        }

        public void AddValues(object[] parts)
        {
            dataTable.Rows.Add(parts);
        }

        public void AddColumn(string name)
        {
            dataTable.Columns.Add(name, typeof(string));
        }

        public void AddColumn(string name, Type type)
        {
            dataTable.Columns.Add(name, type);
        }
    }
}