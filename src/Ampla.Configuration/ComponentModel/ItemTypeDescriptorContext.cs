﻿using System;
using System.ComponentModel;

namespace Ampla.Configuration.ComponentModel
{
    public class ItemTypeDescriptorContext : ITypeDescriptorContext
    {
        public ItemTypeDescriptorContext(object item, PropertyDescriptor propertyDescriptor)
        {
            Instance = item;
            PropertyDescriptor = propertyDescriptor;
            Container = null;
        }


        public object GetService(Type serviceType)
        {
            return null;
        }

        public bool OnComponentChanging()
        {
            return true;
        }

        public void OnComponentChanged()
        {
        }

        public IContainer Container { get; private set; }
        public object Instance { get; private set; }
        public PropertyDescriptor PropertyDescriptor { get; private set; }
    }
}