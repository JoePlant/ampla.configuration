﻿using System.Collections.Generic;
using Ampla.Configuration.Readers;
using Ampla.Configuration.Workers;
using Ampla.Configuration.Workers.Resolvers;

namespace Ampla.Configuration
{
    public class ConfigureProject
    {
        private readonly IProjectFactory projectFactory;
        private readonly ILogger logger;

        public ConfigureProject(IProjectFactory projectFactory, ILogger logger)
        {
            this.projectFactory = projectFactory;
            this.logger = logger;
        }

        public void Execute(string directory, string fileMask)
        {
            AmplaDSLDirectory dslDirectory = new AmplaDSLDirectory(logger, directory, fileMask);

            dslDirectory.Read();

            AmplaDSL[] dsls = dslDirectory.Entries;
            AmplaDSLWorkerFactory factory = new AmplaDSLWorkerFactory();

            List<IProjectWorker> projectWorkers = new List<IProjectWorker>();

            foreach (AmplaDSL amplaDSL in dsls)
            {
                IProjectWorker projectWorker = factory.Create(projectFactory, logger, amplaDSL);
                if (projectWorker != null)
                {
                    projectWorkers.Add(projectWorker);
                }
            }
            
            foreach (IProjectWorker projectWorker in projectWorkers)
            {
                using (projectWorker)
                {
                    logger.LogInformation("Executing " + projectWorker.Name);
                    projectWorker.Execute();
                    logger.LogInformation(projectWorker.Summary());
                }
            }

            IProject project = projectFactory.GetProject();
            if (project.SaveRequired)
            {
                logger.LogInformation("Saving Project");
                project.Save();
                logger.LogInformation("Project saved.");
            }
            else
            {
                logger.LogInformation("Saving Project not required as no changes made.");
            }
        }
    }
}