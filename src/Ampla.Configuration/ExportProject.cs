﻿using System.Collections.Generic;
using System.IO;
using Ampla.Configuration.Readers;
using Ampla.Configuration.Workers;

namespace Ampla.Configuration
{
    public class ExportProject
    {
        private readonly IProjectFactory projectFactory;
        private readonly ILogger logger;

        public ExportProject(IProjectFactory projectFactory, ILogger logger)
        {
            this.projectFactory = projectFactory;
            this.logger = logger;
            SkipTypesNotUsed = true;
        }

        public bool SkipTypesNotUsed { get; set; }

        public void Execute(string directory)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(directory);

            if (!dirInfo.Exists)
            {
                logger.LogWarning("Directory: " + directory + " does not exist.");
            }
            else
            {
                IList<string> itemTypes = projectFactory.GetItemTypes();
                foreach (string itemType in itemTypes)
                {
                    FileInfo exportFile = new FileInfo(Path.Combine(dirInfo.FullName, itemType + ".txt"));
                    if (exportFile.Exists)
                    {
                        exportFile.Delete();
                    }
                    if (exportFile.Exists)
                    {
                        logger.LogWarning(exportFile.FullName + " exists.");
                    }
                    ExportAmplaTypeWorker worker = new ExportAmplaTypeWorker(projectFactory, logger, itemType);
                    worker.Execute();

                    AmplaDSL amplaDSL = worker.AmplaDSL;

                    if (amplaDSL != null)
                    {
                        if (SkipTypesNotUsed)
                        {
                            if (amplaDSL.Values.Rows.Count > 0)
                            {
                                new AmplaDSLWriter(logger, exportFile.FullName, amplaDSL).Write();
                            }
                        }
                        else
                        {
                            new AmplaDSLWriter(logger, exportFile.FullName, amplaDSL).Write();
                        }
                    }
              
                }
            }

        }
    }
}