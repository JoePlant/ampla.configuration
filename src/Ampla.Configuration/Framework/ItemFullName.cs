﻿namespace Ampla.Configuration.Framework
{
    public class ItemFullName
    {
        private readonly string toString;

        public ItemFullName(string parentFullName, string name)
        {
            Name = name;
            if (string.IsNullOrEmpty(parentFullName))
            {
                Parent = null;
                FullName = name;
                toString = "{" + name + "}";
            }
            else
            {
                FullName = parentFullName + "." + name;
                Parent = parentFullName;
                toString = "{" + parentFullName + "}.{" + name + "}";
            }
        }

        public ItemFullName(string fullName)
        {
            FullName = fullName;

            string name;
            string parent;

            ExtractNameAndParent(fullName, out name, out parent);

            Name = name;
            Parent = parent;
            toString = "{" + fullName + "}";
        }

        private static void ExtractNameAndParent(string fullName, out string name, out string parent)
        {
            name = null;
            parent = null;
            string[] parts = fullName.Split('.');

            if (parts.Length == 1)
            {
                name = parts[0];
            }
            else if (parts.Length > 1)
            {
                name = parts[parts.Length-1];
                parent = string.Join(".", parts, 0, parts.Length - 1);
            }
        }

        public string FullName { get; private set; }

        public string Parent { get; private set; }

        public string Name { get; private set; }

        public override string ToString()
        {
            return toString;
        }
    }
}