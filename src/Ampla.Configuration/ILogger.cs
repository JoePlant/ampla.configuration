﻿namespace Ampla.Configuration
{
    public interface ILogger
    {
        void LogVerbose(string message);
        void LogInformation(string message);
        void LogWarning(string message);
    }
}