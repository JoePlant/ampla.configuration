﻿
using System;
using System.ComponentModel;

namespace Ampla.Configuration
{
    public interface IProject : IDisposable
    {
        bool Exists(string fullName);

        object AddChild(string parent, string name);

        object FindItem(string fullName);

        bool SaveRequired { get; }

        void Save();

        bool IsNameValid(string name);

        T GetPropertyValue<T>(string fullName, string property);

        void SetPropertyValue<T>(string fullName, string property, T value);

        PropertyDescriptorCollection GetProperties();

        object[] FindAllItems();

        string GetFullName(object item);
    }

}
