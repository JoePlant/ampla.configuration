﻿using System.Collections.Generic;

namespace Ampla.Configuration
{
    public interface IProjectFactory
    {
        IProject GetProject();

        IProject GetProject(string typeName);

        bool IsValidType(string type);

        IList<string> GetItemTypes();
    }
}