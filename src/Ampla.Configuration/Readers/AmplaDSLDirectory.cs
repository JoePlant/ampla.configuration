﻿using System.Collections.Generic;
using System.IO;

namespace Ampla.Configuration.Readers
{
    public class AmplaDSLDirectory
    {
        private readonly string directory;
        private readonly string fileMask;
        private readonly ILogger logger;

        public AmplaDSLDirectory(ILogger logger, string directory, string fileMask)
        {
            this.logger = logger;
            this.directory = directory;
            Entries = new AmplaDSL[0];
            this.fileMask = fileMask;
        }

        public void Read()
        {
            List<AmplaDSL> entries = new List<AmplaDSL>();
            
            DirectoryInfo dirInfo = new DirectoryInfo(directory);

            if (!dirInfo.Exists)
            {
                logger.LogWarning("Directory: " + directory + " does not exist.");
            }
            else
            {
                int count = 0;
                foreach (FileSystemInfo fileSystemInfo in dirInfo.EnumerateFileSystemInfos(fileMask))
                {
                    count++;
                    AmplaDSLReader reader = new AmplaDSLReader(logger, fileSystemInfo.FullName);
                    reader.Read();
                    if (reader.Entry != null)
                    {
                        entries.Add(reader.Entry);
                    }
                }
                if (count == 0)
                {
                    logger.LogWarning("No files match : " + fileMask + " in directory: " + directory);
                }
            }

            Entries = entries.ToArray();
        }

        public AmplaDSL[] Entries { get; private set; }
    }
}