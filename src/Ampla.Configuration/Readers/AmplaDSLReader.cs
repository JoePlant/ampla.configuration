﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Ampla.Configuration.Readers
{
    public class AmplaDSLReader
    {
        private readonly string fileName;
        private static readonly string[] HeaderSplit = new [] {headerDelimer};
        private const string headerDelimer = ":=";
        private const string endHeader = "===";
        private Dictionary<string, Type> registeredTypes;
        private readonly ILogger logger;

        public AmplaDSLReader(ILogger logger, string fileName)
        {
            this.logger = logger;
            this.fileName = fileName;
        }

        public void Read()
        {
            using (StreamReader streamReader = new StreamReader(fileName))
            {
                AmplaDSL dsl = new AmplaDSL {Source = fileName};
                bool inColumns = false;
                bool headerFinished = false;
                bool inValues = false;
                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    if (line != null)
                    {
                        if (!headerFinished && line.StartsWith(endHeader))
                        {
                            headerFinished = true;
                        }
                        else if (!headerFinished && line.Contains(headerDelimer))
                        {
                            ReadHeader(line, dsl);
                        }
                        else if (headerFinished && !inColumns)
                        {
                            inColumns = true;
                            ReadColumns(line, dsl);
                            inValues = true;
                        }
                        else if (inValues)
                        {
                            readValues(line, dsl);
                        }
                    }
                }
                Entry = dsl;
            }
        }

        public AmplaDSL Entry { get; private set; }

        private void ReadColumns(string line, AmplaDSL dsl)
        {
            string[] parts = line.Split(',');
            for (int i = 0; i < parts.Length; i++)
            {
                string column = parts[i];
                if (column.Contains(":"))
                {
                    string[] nameParts = column.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries);
                    if (nameParts.Length == 2)
                    {
                        Type type = CheckType(nameParts[1]);
                        string name = nameParts[0];
                        dsl.AddColumn(name, type);
                    }
                }
                else
                {
                    dsl.AddColumn(column, typeof(string));
                }
            }
        }

        private Type CheckType(string type)
        {
            if (registeredTypes == null)
            {
                registeredTypes = new Dictionary<string, Type>(StringComparer.InvariantCultureIgnoreCase);
                registeredTypes.Add("int", typeof(int));
                registeredTypes.Add("double", typeof(double));
            }
            Type result;
            if (registeredTypes.TryGetValue(type, out result))
            {
                return result;
            }
            return typeof (string);
        }

        private void ReadHeader(string line, AmplaDSL dsl)
        {
            string[] parts = line.Split(HeaderSplit, StringSplitOptions.None);
            if (parts.Length > 1)
            {
                string header = parts[0];
                string value = parts.Length > 2 ? string.Join(headerDelimer, parts, 1, parts.Length - 1) : parts[1];
                if (!dsl.SetHeader(header, value))
                {
                    logger.LogWarning("Invalid Header: " + line);
                }
            }
        }

        private void readValues(string line, AmplaDSL dsl)
        {
            object[] parts = new List<object>(line.Split(',')).ToArray();
            dsl.AddValues(parts);
        }
    }
}