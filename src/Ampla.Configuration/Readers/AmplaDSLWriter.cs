﻿using System.Data;
using System.IO;
using System.Linq;

namespace Ampla.Configuration.Readers
{
    public class AmplaDSLWriter
    {
        private readonly string fileName;
        private const string headerDelimer = ":=";
        private const string endHeader = "===";
        private readonly ILogger logger;
        private readonly AmplaDSL amplaDSL;

        public AmplaDSLWriter(ILogger logger, string fileName, AmplaDSL amplaDSL)
        {
            this.logger = logger;
            this.fileName = fileName;
            this.amplaDSL = amplaDSL;
        }

        public void Write()
        {
            using (StreamWriter streamWriter = new StreamWriter(fileName))
            {
                WriteHeader(amplaDSL, streamWriter);
                WriteValues(amplaDSL, streamWriter);
            }

            logger.LogInformation("Ampla DSL exported to " + fileName);
        }

        private static void WriteValues(AmplaDSL entry, StreamWriter streamWriter)
        {
            DataTable table = entry.Values;
            
            if (table.Columns.Count > 0)
            {
                var columns = (from DataColumn column in table.Columns select column.ColumnName);
                streamWriter.WriteLine(string.Join(",", columns));

                foreach (DataRow row in table.Rows)
                {
                    streamWriter.WriteLine(string.Join(",", row.ItemArray));
                }
            }
        }

        private static void WriteHeader(AmplaDSL entry, StreamWriter streamWriter)
        {
            if (!string.IsNullOrEmpty(entry.Type))
            {
                streamWriter.WriteLine("{0}{1}{2}", "Type", headerDelimer, entry.Type);
            }
            else
            {
                streamWriter.WriteLine("{0}{1}", "Type", headerDelimer);
            }

            if (!string.IsNullOrEmpty(entry.Mode))
            {
                streamWriter.WriteLine("{0}{1}{2}", "Mode", headerDelimer, entry.Mode);
            }

            if (!string.IsNullOrEmpty(entry.Parent))
            {
                streamWriter.WriteLine("{0}{1}{2}", "Parent", headerDelimer, entry.Parent);
            }
            if (!string.IsNullOrEmpty(entry.FindType))
            {
                streamWriter.WriteLine("{0}{1}{2}", "FindType", headerDelimer, entry.FindType);
            }
            streamWriter.WriteLine(endHeader);
        }
    }
}