﻿using System.ComponentModel;
using System.Data;

namespace Ampla.Configuration.Workers
{
    public class AddChildItemsWorker : AmplaDSLWorker
    {
        public AddChildItemsWorker(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
            : base(projectFactory, logger, amplaDSL)
        {
            var columns = amplaDSL.GetColumns();
            if (columns.Count > 1)
            {
                string properties = string.Join(",", columns);
                Name = string.Format("AddChildItems Count:{0} Parent:='{1}' Type:={2} Properties:='{3}'",
                                     amplaDSL.Values.Rows.Count, amplaDSL.Parent, amplaDSL.Type, properties);
            }
            else
            {
                Name = string.Format("AddChildItems Count:{0} Parent:='{1}' Type:={2}", amplaDSL.Values.Rows.Count,
                                     amplaDSL.Parent, amplaDSL.Type);
            }
        }

        public override void Execute()
        {
            IProject projectItem = Project.GetProject();

            if (EnsureModeIsCorrect("Add"))
            {
                string parentFullName = AmplaDSL.Parent;
                if (parentFullName != null)
                {
                    if (projectItem.Exists(parentFullName))
                    {
                        Logger.LogVerbose("Parent: " + parentFullName);
                        string itemType = AmplaDSL.Type;
                        if (Project.IsValidType(itemType))
                        {
                            IProject projectChild = Project.GetProject(itemType);
                            if (projectChild != null)
                            {
                                PropertyDescriptorCollection propertyDescriptors = projectChild.GetProperties();
                                if (PropertiesAreValid(propertyDescriptors, Logger, AmplaDSL, "Name"))
                                {
                                    foreach (DataRow dataRow in AmplaDSL.Values.Rows)
                                    {
                                        IncrementCount();
                                        string name = GetRowValue<string>(dataRow, "Name");
                                        object item = AddChildItem(projectChild, parentFullName, name);
                                        if (item != null)
                                        {
                                            SetProperties(item, propertyDescriptors, dataRow);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            LogError("Invalid Type: {0}.", AmplaDSL.Type);
                        }
                    }
                    else
                    {
                        LogError("Parent: {0} does not exist.", parentFullName);
                    }
                }
                else
                {
                    LogError("Parent header is not specified.");
                }
            }
        }
    }
}