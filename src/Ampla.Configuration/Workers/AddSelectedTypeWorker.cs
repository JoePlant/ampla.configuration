﻿using System.ComponentModel;
using System.Data;

namespace Ampla.Configuration.Workers
{
    public class AddSelectedTypeWorker : AmplaDSLWorker
    {
        public AddSelectedTypeWorker(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
            : base(projectFactory, logger, amplaDSL)
        {
            var columns = amplaDSL.GetColumns();
            if (columns.Count > 1)
            {
                string properties = string.Join(",", columns);
                Name = string.Format("AddSelectedType Count:{0} FindType:={1} Type:={2} Properties:='{3}'",
                                     amplaDSL.Values.Rows.Count,
                                     amplaDSL.FindType,
                                     amplaDSL.Type,
                                     properties);
            }
            else
            {
                Name = string.Format("AddSelectedType Count:{0} FindType:={1} Type:={2}",
                                     amplaDSL.Values.Rows.Count,
                                     amplaDSL.FindType,
                                     amplaDSL.Type);
            }
        }

        public override void Execute()
        {
            IProject projectItem = Project.GetProject();

            if (EnsureModeIsCorrect("Add"))
            {
                string parentFullName = AmplaDSL.Parent;
                if (string.IsNullOrEmpty(parentFullName))
                {
                    string findType = AmplaDSL.FindType;

                    if (Project.IsValidType(findType))
                    {
                        IProject projectSelectType = Project.GetProject(findType);
                        string itemType = AmplaDSL.Type;
                        if (Project.IsValidType(itemType))
                        {
                            IProject projectChild = Project.GetProject(itemType);
                            
                            if (projectChild != null)
                            {
                                PropertyDescriptorCollection propertyDescriptors = projectChild.GetProperties();
                                if (PropertiesAreValid(propertyDescriptors, Logger, AmplaDSL, "RelativeName"))
                                {
                                    object[] foundItems = projectSelectType.FindAllItems();

                                    foreach (object foundItem in foundItems)
                                    {
                                        foreach (DataRow dataRow in AmplaDSL.Values.Rows)
                                        {
                                            IncrementCount();
                                            string relativeName = GetRowValue<string>(dataRow, "RelativeName");

                                            object item = AddRelativeItem(projectChild, foundItem, relativeName);
                                            if (item != null)
                                            {
                                                SetProperties(item, propertyDescriptors, dataRow);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            LogError("Type: {0} is not valid.", itemType);
                        }
                    }
                    else
                    {
                        LogError("FindType: {0} is not valid.", findType);
                    }
                }
                else
                {
                    LogError("Parent: '{0}' should not be specified.", parentFullName);
                }
            }

        }


    }
}