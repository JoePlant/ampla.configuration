﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using Ampla.Configuration.ComponentModel;
using Ampla.Configuration.Framework;

namespace Ampla.Configuration.Workers
{
    public abstract class AmplaDSLWorker : IProjectWorker
    {
        private readonly StringBuilder messages = new StringBuilder();
        private bool hasErrors ;

        private int addedItems;
        private int count;
        private int invalidNames;
        private int existingItems;

        private readonly static List<string> IgnoreProperties = new List<string>() {"Name", "FullName"}; 

        protected AmplaDSLWorker(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
        {
            Project = projectFactory;
            Logger = logger;
            AmplaDSL = amplaDSL;
        }

        protected IProjectFactory Project
        {
            get; private set;
        }

        protected ILogger Logger { get; private set; }

        protected AmplaDSL AmplaDSL { get; private set; }

        public abstract void Execute();

        public string Name { get; protected set; }

        protected void LogError(string format, params object[] args)
        {
            string message = string.Format(format, args);
            messages.AppendLine("ERROR: " + message);
            Logger.LogWarning(message);
            hasErrors = true;
        }

        protected bool EnsureModeIsCorrect(string expectedMode)
        {
            if (AmplaDSL.Mode != expectedMode)
            {
                LogError("Incorrect Mode Property 'Mode:={0}'. Should be 'Mode:={1}'", AmplaDSL.Mode, expectedMode);
                return false;
            }
            return true;
        }

        protected void SetProperties(object item, PropertyDescriptorCollection properties, DataRow dataRow)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            foreach (DataColumn column in dataRow.Table.Columns)
            {
                if (!IgnoreProperties.Contains(column.ColumnName))
                {
                    PropertyDescriptor property = properties[column.ColumnName];
                    if (property != null)
                    {
                        object value = dataRow[column];
                        SetProperty(property, item, value);
                    }
                }
            }
        }

        protected void SetProperty(PropertyDescriptor property, object item, object value)
        {
            if (value is DBNull)
            {
                ResetPropertyValue(property, item);
            }
            else if (value.GetType() == property.PropertyType)
            {
                SetPropertyValue(property, item, value);
            }
            else
            {
                TypeConverter typeConverter = property.Converter;
                if (typeConverter != null)
                {
                    ITypeDescriptorContext context = new ItemTypeDescriptorContext(item, property);
                    if (value is string && typeConverter.CanConvertFrom(context, typeof(string)))
                    {
                        object newValue = typeConverter.ConvertFromInvariantString(context, (string)value);
                        if (newValue != null)
                        {
                            SetPropertyValue(property, item, newValue);
                        }
                        else
                        {
                            ResetPropertyValue(property, item);
                        }
                    }
                    else
                    {
                        string message = string.Format("TypeConverter {0} is not able to convert value ('{1}') for property: {2} on {3}", typeConverter.GetType().FullName, value, property.Name, item);
                        Logger.LogWarning(message);
                    }
                }
                else
                {
                    string message = string.Format("No TypeConverter available to convert value ('{0}') for property: {1} on {2}", value, property.Name, item);
                    Logger.LogWarning(message);
                }
            }
        }

        private void SetPropertyValue(PropertyDescriptor property, object item, object value)
        {
            property.SetValue(item, value);
            string message = string.Format("Setting Property: {1} = {2} on {0}", item, property.Name, value);
            Logger.LogVerbose(message);
        }

        private void ResetPropertyValue(PropertyDescriptor property, object item)
        {
            if (property.CanResetValue(item))
            {
                string message = string.Format("Resetting property: {0} on {1}", property.Name, item);
                Logger.LogInformation(message);
                property.ResetValue(item);
            }
        }

        protected void IncrementCount()
        {
            count++;
        }

        protected object AddChildItem(IProject project, string parentFullName, string name)
        {
            object item = null;

            string fullName = parentFullName + "." + name;
            if (!project.Exists(fullName))
            {
                if (project.IsNameValid(name))
                {
                    item = project.AddChild(parentFullName, name);
                    Logger.LogInformation("Added " + fullName);
                    addedItems++;
                }
                else
                {
                    Logger.LogWarning("Invalid Name: " + fullName);
                    invalidNames++;
                }
            }
            else
            {
                Logger.LogVerbose(fullName + " exists");
                existingItems++;
            }
            return item;
        }

        protected object FindChildItem(IProject project, string parentFullName, string name)
        {
            ItemFullName fullName = new ItemFullName(parentFullName, name);

            if (project.Exists(fullName.FullName))
            {
                return FindItem(project, fullName.FullName);
            }
            return null;
        }

        protected object AddRelativeItem(IProject project, object parentItem, string relativeName)
        {
            string parentFullName = project.GetFullName(parentItem);
            string itemFullName = parentFullName + "." + relativeName;
            return AddItem(project, itemFullName);
        }

        protected object AddItem(IProject project, string fullName)
        {
            object item = null;
            if (!project.Exists(fullName))
            {
                ItemFullName itemFullName = new ItemFullName(fullName);
                if (string.IsNullOrEmpty(itemFullName.Parent) || project.Exists(itemFullName.Parent))
                {
                    if (project.IsNameValid(itemFullName.Name))
                    {
                        item = project.AddChild(itemFullName.Parent, itemFullName.Name);
                        Logger.LogInformation("Added " + fullName);
                        addedItems++;
                    }
                    else
                    {
                        Logger.LogWarning("Invalid Name: " + fullName);
                        invalidNames++;
                    }
                }
                else
                {
                    LogError(itemFullName.Parent + " does not exist.");
                }
            }
            else
            {
                Logger.LogVerbose(fullName + " exists");
                existingItems++;
            }
            return item;
        }

        protected object FindItem(IProject project, string fullName)
        {
            object item = null;
            if (project.Exists(fullName))
            {
                item = project.FindItem(fullName);
            }
            return item;
        }

        protected T GetRowValue<T>(DataRow dataRow, string column)
        {
            if (dataRow.Table.Columns.Contains(column))
            {
                return (T) dataRow[column];
            }

            LogError("Unable to find column: {0}", column);

            return default(T);
        }

        protected bool PropertiesAreValid(PropertyDescriptorCollection propertyDescriptors, ILogger logger, AmplaDSL amplaDSL, string ignoreColumn)
        {
            bool valid = true;
            IList<string> columns = amplaDSL.GetColumns();
            if (columns.Count > 1)
            {
                for (int i = 0; i < columns.Count; i++)
                {
                    string column = columns[i];
                    if (column != ignoreColumn)
                    {
                        PropertyDescriptor property = propertyDescriptors[column];
                        if (property == null)
                        {
                            valid = false;
                            string message = string.Format("Unable to find property: '{0}' on type: '{1}'", column,
                                                           amplaDSL.Type);
                            logger.LogWarning(message);
                        }
                        if (property != null && property.IsReadOnly)
                        {
                            valid = false;
                            string message = string.Format("Property: '{0}' is readonly on type: '{1}'", column,
                                                           amplaDSL.Type);
                            logger.LogWarning(message);
                        }
                    }
                }
            }
            if (!valid)
            {
                LogError("At least one of these properties are not valid: {0}", string.Join(",", amplaDSL.GetColumns()));
            }
            return valid;
        }

        public string Summary()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(Name);
            if (hasErrors)
            {
                builder.Append(messages);
            }
            else
            {
                builder.AppendFormat("  Existing items: {0}", existingItems);
                builder.AppendLine();
                builder.AppendFormat("  Added items: {0}", addedItems);
                builder.AppendLine();
                builder.AppendFormat("  Invalid items: {0}", invalidNames);
                builder.AppendLine();
                builder.AppendFormat(" Total: {0}", count);
            }
            return builder.ToString();
        }

        public void Dispose()
        {
            Project = null;
        }
    }}