﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

namespace Ampla.Configuration.Workers
{
    public class ExportAmplaTypeWorker : IProjectWorker
    {
        private readonly StringBuilder messages = new StringBuilder();
        private bool hasErrors ;

        private int addedItems;
        private int count;
        private int invalidNames;
        private int existingItems;

        public ExportAmplaTypeWorker(IProjectFactory projectFactory, ILogger logger, string itemType )
        {
            Project = projectFactory;
            Logger = logger;
            ItemType = itemType;
            AmplaDSL = null;
        }

        protected string ItemType { get; private set; }

        public AmplaDSL AmplaDSL { get; private set; }

        protected IProjectFactory Project
        {
            get; private set;
        }

        protected ILogger Logger { get; private set; }

        public void Execute()
        {
            if (Project.IsValidType(ItemType))
            {
                AmplaDSL = new AmplaDSL();
                AmplaDSL.Type = ItemType;
                AmplaDSL.Mode = "Export";
                AmplaDSL.Parent = null;
                AmplaDSL.FindType = null;
                AmplaDSL.AddColumn("FullName");
                IProject project = Project.GetProject(AmplaDSL.Type);
                if (project != null)
                {
                    List<PropertyDescriptor> listProperties = new List<PropertyDescriptor>();
                    foreach (PropertyDescriptor property in project.GetProperties().Sort())
                    {
                        if (!property.IsReadOnly)
                        {
                            TypeConverter converter = property.Converter;
                            if (converter != null)
                            {
                                if (converter.CanConvertFrom(typeof (string)) && converter.CanConvertTo(typeof(string)))
                                {
                                    listProperties.Add(property);
                                    AmplaDSL.AddColumn(property.Name, typeof(string));
                                }
                            }
                            else
                            {
                                Logger.LogWarning("Property: " + property.Name + " (No TypeConverter)");
                            }
                        }
                        else
                        {
                            Logger.LogVerbose("Property: " + property.Name + " (ReadOnly)");
                        }
                    }
                    
                    object[] items = project.FindAllItems();
                    foreach (object item in items)
                    {
                        List<object> valueList = new List<object>();
                        valueList.Add(project.GetFullName(item));
                        foreach (PropertyDescriptor property in listProperties)
                        {
                            if (property.ShouldSerializeValue(item))
                            {
                                object value = property.GetValue(item);
                                TypeConverter converter = property.Converter;
                                if ((converter != null) && (value != null))
                                {
                                    valueList.Add(converter.ConvertToInvariantString(value));
                                }
                                else
                                {
                                    valueList.Add(DBNull.Value);
                                }
                            }
                            else
                            {
                                valueList.Add(DBNull.Value);
                            }
                        }
                        AmplaDSL.AddValues(valueList.ToArray());
                    }
                }
            }
            else
            {
                LogError("Type: {0} is not valid", ItemType);
            }
        }

        public void WriteType(TextWriter writer)
        {
            writer.WriteLine("{0}:={1}", "Type", ItemType);
        }

        public string Name { get; protected set; }

        protected void LogError(string format, params object[] args)
        {
            string message = string.Format(format, args);
            messages.AppendLine("ERROR: " + message);
            Logger.LogWarning(message);
            hasErrors = true;
        }

        public string Summary()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(Name);
            if (hasErrors)
            {
                builder.Append(messages);
            }
            else
            {
                builder.AppendFormat("  Existing items: {0}", existingItems);
                builder.AppendLine();
                builder.AppendFormat("  Added items: {0}", addedItems);
                builder.AppendLine();
                builder.AppendFormat("  Invalid items: {0}", invalidNames);
                builder.AppendLine();
                builder.AppendFormat(" Total: {0}", count);
            }
            return builder.ToString();
        }

        public void Dispose()
        {
            Project = null;
        }
    }}