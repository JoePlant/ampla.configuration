﻿using System;

namespace Ampla.Configuration.Workers
{
    public interface IProjectWorker : IDisposable
    {
        void Execute();
        string Summary();
        string Name { get; }
    }
}