﻿using System.ComponentModel;
using System.Data;
using Ampla.Configuration.ComponentModel;
using Ampla.Configuration.Framework;

namespace Ampla.Configuration.Workers
{
    public class ModifyChildItemPropertiesWorker : AmplaDSLWorker
    {
        public ModifyChildItemPropertiesWorker(IProjectFactory project, ILogger logger, AmplaDSL amplaDSL)
            : base(project, logger, amplaDSL)
        {
            var columns = amplaDSL.GetColumns();
            if (columns.Count > 1)
            {
                string properties = string.Join(",", columns);
                Name = string.Format("ModifyChildItemProperties Count:{0} Type:={1} Properties:={2}",
                                     amplaDSL.Values.Rows.Count, amplaDSL.Type, properties);
            }
            else
            {
                Name = string.Format("ModifyChildItemProperties Count:{0} Type:={1}", amplaDSL.Values.Rows.Count,
                                     amplaDSL.Type);
            }
        }

        public override void Execute()
        {
            IProject allItems = Project.GetProject();

            if (EnsureModeIsCorrect("Modify"))
            {
                string parentFullName = AmplaDSL.Parent;
                if (parentFullName != null)
                {
                    if (allItems.Exists(parentFullName))
                    {
                        Logger.LogVerbose("Parent: " + parentFullName);
                        string itemType = AmplaDSL.Type;
                        if (Project.IsValidType(itemType))
                        {
                            IProject projectChild = Project.GetProject(itemType);
                            if (projectChild != null)
                            {
                                PropertyDescriptorCollection propertyDescriptors = projectChild.GetProperties();
                                if (PropertiesAreValid(propertyDescriptors, Logger, AmplaDSL, "Name"))
                                {
                                    foreach (DataRow dataRow in AmplaDSL.Values.Rows)
                                    {
                                        IncrementCount();
                                        string name = GetRowValue<string>(dataRow, "Name");
                                        ItemFullName itemFullName = new ItemFullName(parentFullName, name);

                                        object item = FindItem(projectChild, itemFullName.FullName);
                                        if (item != null)
                                        {
                                            SetProperties(item, propertyDescriptors, dataRow);
                                        }
                                        else
                                        {
                                            if (allItems.FindItem(itemFullName.FullName) != null)
                                            {
                                                Logger.LogWarning(itemFullName.FullName + " exists but is not of type: " +
                                                                  itemType);
                                            }
                                            else
                                            {
                                                Logger.LogWarning(itemFullName.FullName + " does not exist.");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            LogError("Invalid Type: {0}.", AmplaDSL.Type);
                        }
                    }
                    else
                    {
                        LogError("Parent: {0} does not exist.", parentFullName);
                    }
                }
                else
                {
                    LogError("Parent header is not specified.");
                }
            }
        }
    }
}