﻿using System.ComponentModel;
using System.Data;

namespace Ampla.Configuration.Workers
{
    public class ModifyFullNamePropertiesWorker : AmplaDSLWorker
    {
        public ModifyFullNamePropertiesWorker(IProjectFactory project, ILogger logger, AmplaDSL amplaDSL)
            : base(project, logger, amplaDSL)
        {
            var columns = amplaDSL.GetColumns();
            if (columns.Count > 1)
            {
                string properties = string.Join(",", columns);
                Name = string.Format("ModifyProperties Count:{0} Type:={1} Properties:={2}", amplaDSL.Values.Rows.Count, amplaDSL.Type, properties);
            }
            else
            {
                Name = string.Format("ModifyProperties Count:{0} Type:={1}", amplaDSL.Values.Rows.Count, amplaDSL.Type);
            }
        }
        
        public override void Execute()
        {
            string parentFullName = AmplaDSL.Parent;

            if (EnsureModeIsCorrect("Modify"))
            {
                if (parentFullName == null)
                {
                    if (Project.IsValidType(AmplaDSL.Type))
                    {
                        IProject project = Project.GetProject(AmplaDSL.Type);
                        IProject allItems = Project.GetProject();
                        if (project != null)
                        {
                            PropertyDescriptorCollection properties = project.GetProperties();
                            if (PropertiesAreValid(properties, Logger, AmplaDSL, "FullName"))
                            {
                                foreach (DataRow dataRow in AmplaDSL.Values.Rows)
                                {
                                    IncrementCount();
                                    string fullName = GetRowValue<string>(dataRow, "FullName");
                                    object item = FindItem(project, fullName);
                                    if (item != null)
                                    {
                                        SetProperties(item, properties, dataRow);
                                    }
                                    else
                                    {
                                        object foundItem = FindItem(allItems, fullName);
                                        if (foundItem != null)
                                        {
                                            Logger.LogWarning("'" + fullName + "' is not the correct type. Expected: " +
                                                              AmplaDSL.Type);
                                        }
                                        else
                                        {
                                            Logger.LogWarning("Unable to find item: " + fullName);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        LogError("Invalid Type: {0}.", AmplaDSL.Type);
                    }
                }
                else
                {
                    LogError("Parent: {0} should not be specified when Modifying FullName items.", parentFullName);
                }
            }
        }


    }
}