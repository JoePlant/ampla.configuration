﻿namespace Ampla.Configuration.Workers.Resolvers
{
    public class AddChildWorkerResolver : WorkerResolver
    {
        public override bool IsMatch(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
        {
            if (IsParentSpecified(amplaDSL) && !IsFindTypeSpecified(amplaDSL))
            {
                if (IsMode(amplaDSL, "Add"))
                {
                    if (IsValidType(projectFactory, amplaDSL))
                    {
                        if (IsFirstColumn(amplaDSL, "Name"))
                        {
                            if (!ContainsColumn(amplaDSL, "FullName"))
                            {
                                return true;
                            }
                            LogWarning(logger,
                                       "Source: '{0}'. If Parent is specified, then only a Name column can be used.  Remove the FullName column.",
                                       amplaDSL.Source);
                        }
                        else
                        {
                            LogWarning(logger,
                                       "Source: '{0}'. If parent is specified then Name column must be in the first column",
                                       amplaDSL.Source);
                        }
                    }
                }
            }
            return false;
        }


        public override IProjectWorker CreateWorker(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
        {
            return new AddChildItemsWorker(projectFactory, logger, amplaDSL);
        }
    }
}