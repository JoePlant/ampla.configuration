﻿namespace Ampla.Configuration.Workers.Resolvers
{
    public class AddFullNameWorkerResolver : WorkerResolver
    {
        public override bool IsMatch(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
        {
            if (!IsParentSpecified(amplaDSL) && !IsFindTypeSpecified(amplaDSL))
            {
                if (IsMode(amplaDSL, "Add"))
                {
                    if (IsValidType(projectFactory, amplaDSL))
                    {
                        if (IsFirstColumn(amplaDSL, "FullName"))
                        {
                            if (!ContainsColumn(amplaDSL, "Name"))
                            {
                                return true;
                            }
                            LogWarning(logger,
                                       "Source: '{0}'. Both Name and FullName can't be specified together.  Remove the Name column or add a Parent item.",
                                       amplaDSL.Source);
                        }
                        else
                        {
                            LogWarning(logger, "Source: '{0}'. The FullName column must be in the first column",
                                       amplaDSL.Source);
                        }
                    }
                }
            }
            return false;
        }
        
        public override IProjectWorker CreateWorker(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
        {
            return new AddFullNameItemsWorker(projectFactory, logger, amplaDSL);
        }
    }
}