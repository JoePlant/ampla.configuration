﻿namespace Ampla.Configuration.Workers.Resolvers
{
    public class AddSelectedTypeWorkerResolver : WorkerResolver
    {
        public override bool IsMatch(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
        {
            if (IsFindTypeSpecified(amplaDSL))
            {
                if (!IsParentSpecified(amplaDSL))
                {
                    if (IsMode(amplaDSL, "Add"))
                    {
                        if (IsValidType(projectFactory, amplaDSL))
                        {
                            if (IsFirstColumn(amplaDSL, "RelativeName"))
                            {
                                if (!ContainsColumn(amplaDSL, "Name"))
                                {
                                    if (!ContainsColumn(amplaDSL, "FullName"))
                                    {
                                        return true;
                                    }
                                    LogWarning(logger,
                                               "Source: '{0}'. If FindType is specified, then only a RelativeName column can be used.  Remove the FullName column.",
                                               amplaDSL.Source);
                                }
                                else
                                {
                                    LogWarning(logger,
                                               "Source: '{0}'. If FindType is specified, then only a RelativeName column can be used.  Remove the Name column.",
                                               amplaDSL.Source);
                                }
                            }
                            else
                            {
                                LogWarning(logger,
                                           "Source: '{0}'. If FindType is specified then RelativeName column must be in the first column",
                                           amplaDSL.Source);
                            }
                        }
                    }
                }
                else
                {
                    LogWarning(logger,
                               "Source: '{0}'. If FindType is specified then Parent can not be used",
                               amplaDSL.Source);
                }
            }
            return false;
        }


        public override IProjectWorker CreateWorker(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
        {
            return new AddSelectedTypeWorker(projectFactory, logger, amplaDSL);
        }
    }
}