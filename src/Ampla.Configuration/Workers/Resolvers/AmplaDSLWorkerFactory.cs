﻿using System.Collections.Generic;
using System.Linq;

namespace Ampla.Configuration.Workers.Resolvers
{
    public class AmplaDSLWorkerFactory
    {
        private readonly List<WorkerResolver> workerResolvers = new List<WorkerResolver> {
            new AddChildWorkerResolver(), new AddFullNameWorkerResolver(), 
            new ModifyFullNameWorkerResolver(), new ModifyChildWorkerResolver(),
            new AddSelectedTypeWorkerResolver(),
            new InvalidTypeWorkerResolver(), new InvalidModeWorkerResolver()};


        public IProjectWorker Create(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
        {
            return (
                       from resolver
                           in workerResolvers
                       where resolver.IsMatch(projectFactory, logger, amplaDSL)
                       select resolver.CreateWorker(projectFactory, logger, amplaDSL)
                   )
                .FirstOrDefault();
        }
    }
}