﻿namespace Ampla.Configuration.Workers.Resolvers
{
    public class InvalidModeWorkerResolver : WorkerResolver
    {
        public override bool IsMatch(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
        {
            if (!IsValidMode(amplaDSL))
            {
                LogInvalidModeMessage(logger, amplaDSL);
                return true;
            }

            return false;
        }

        public override IProjectWorker CreateWorker(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
        {
            return null;
        }
    }
}