﻿namespace Ampla.Configuration.Workers.Resolvers
{
    public class InvalidTypeWorkerResolver : WorkerResolver
    {
        public override bool IsMatch(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
        {
            if (string.IsNullOrEmpty(amplaDSL.Type))
            {
                LogWarning(logger, "Source: '{0}'. A Type must be specified.\r\ne.g.\r\n  Type:=Citect.Ampla.General.Server.ApplicationsFolder", amplaDSL.Source);
                return true;
            }

            if (!IsValidType(projectFactory, amplaDSL))
            {
                LogWarning(logger, "Source: '{0}'. The Type: '{1}' is not valid.", amplaDSL.Source, amplaDSL.Type);
                return true;
            }

            if (IsFindTypeSpecified(amplaDSL) && !IsValidFindType(projectFactory, amplaDSL))
            {
                LogWarning(logger, "Source: '{0}'. The FindType: '{1}' is not valid.", amplaDSL.Source, amplaDSL.Type);
                return true;
            }
            return false;
        }

        public override IProjectWorker CreateWorker(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL)
        {
            return null;
        }
    }
}