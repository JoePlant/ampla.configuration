﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Ampla.Configuration.Workers.Resolvers
{
    public abstract class WorkerResolver
    {
        public abstract bool IsMatch(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL);
        
        public abstract IProjectWorker CreateWorker(IProjectFactory projectFactory, ILogger logger, AmplaDSL amplaDSL);

        private static readonly List<string> ValidModes = new List<string> { "Add", "Modify", "Delete" };

        protected bool IsValidType(IProjectFactory projectFactory, AmplaDSL amplaDSL)
        {
            return !string.IsNullOrEmpty(amplaDSL.Type) && projectFactory.IsValidType(amplaDSL.Type);
        }

        protected bool IsValidMode(AmplaDSL amplaDSL)
        {
            return ValidModes.Contains(amplaDSL.Mode);
        }

        protected static void LogInvalidModeMessage(ILogger logger, AmplaDSL amplaDSL)
        {
            LogWarning(logger,
                       "Mode specified ('{0}') is not valid. Valid modes are {1} Source:{3} .\r\ne.g.\r\n  Mode:={2}",
                       amplaDSL.Mode, string.Join(",", ValidModes), ValidModes[0], amplaDSL.Source);
        }

        protected static void LogWarning(ILogger logger, string format, params object[] args)
        {
            string message = string.Format(format, args);
            logger.LogWarning(message);
        }

        protected bool IsParentSpecified(AmplaDSL amplaDSL)
        {
            bool parentSpecified = !string.IsNullOrEmpty(amplaDSL.Parent);
            return parentSpecified;
        }

        protected bool IsFindTypeSpecified(AmplaDSL amplaDSL)
        {
            bool findTypeSpecified = !string.IsNullOrEmpty(amplaDSL.FindType);
            return findTypeSpecified;
        }

        protected bool IsValidFindType(IProjectFactory projectFactory, AmplaDSL amplaDSL)
        {
            return !string.IsNullOrEmpty(amplaDSL.FindType) && projectFactory.IsValidType(amplaDSL.FindType);
        }

        protected bool IsFirstColumn(AmplaDSL amplaDSL, string columnName)
        {
            DataTable dataTable = amplaDSL.Values;
            if (dataTable != null && dataTable.Columns.Count > 0)
            {
                string dataColumn = dataTable.Columns[0].ColumnName;
                return StringComparer.InvariantCulture.Compare(dataColumn, columnName) == 0;
            }
            return false;
        }

        protected bool IsMode(AmplaDSL amplaDSL, string mode)
        {
            return amplaDSL.Mode == mode;
        }

        protected bool ContainsColumn(AmplaDSL amplaDSL, string columnName)
        {
            DataTable dataTable = amplaDSL.Values;
            if (dataTable != null && dataTable.Columns.Count > 0)
            {
                DataColumn dataColumn = dataTable.Columns[columnName];
                return dataColumn != null;
            }
            return false;
        }


    }
}